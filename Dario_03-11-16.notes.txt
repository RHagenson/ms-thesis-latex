Points of conversation for Dario meeting 03-11-16

1. Help identifying bug that stops cycle_cancers.sh after the first 
non-processed cancer. See 25-10-16.out and 25-10-16.err in ms-thesis/

Took out source command call, will see if it processes more KIRC


2. Can I get/where can I find the exact pipeline Dario used to generate 
the disorder scores? I want to run a secondary analysis to compare how 
the mutations affect these values.

May be a different paper, this current analysis is more on the side of 
how the mutations affect binding affinity in disorder regions

Look up the SIFT score

This may be looking more at the stability of the protein.

Analyzing the 18 different mutations (1 being the mutation occuring and 
1 being the actual/standard amino acid present)


3. have corrected cutoff be those below 0.1


4. proximity of mutations should be taken into account in a later stae


do cancers also mutate docking sites? (RQ)

In unstrurctued regions, those near in sequence are near in 3D space in 
folded protein

Have the corrected data.frame also output the observed and average 
disorder along with the magnitude of difference (obs/average)

The current pipeline does the effectively distinguish between clustered 
mutations in one region of disorder and mutations in separate regions of 
disorder. This is due to the oberserved disorder remaining the same, and 
of course, the average being calculated the same.
