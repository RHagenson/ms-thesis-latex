% Chapter Template

\chapter{Positional Analysis Results} % Main chapter title

\label{Ch-Positional} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Introduction}
Monte Carlo simulation results across the $31$ cancer types (listed in \tbref{Tab-TCGA_Cancers}) were limited to only considering IUPred short findings. IUPred short is better able to capture regions of positional disorder by considering a localized proximity window size of $25$ residues. When considering only genes with $5$ or more observed mutations, furthering the conservative estimation of significance, $102$ significant genes were found. Well-characterized driver genes were removed by taking the set difference between the COSMIC gene set and this significant set. This left $77$ remaining gene symbols across both missense-only and all-mutations profiles. See \tbref{Tab-PosCosmicMatrix} for a listing of these finds or \fgref{Fig-PosFindsHeatmap} for a binary mapping of these finds to the cancer profiles they were significant in.

%----------------------------------------------------------------------------------------
%	COSMIC Set Difference as Matrix
%----------------------------------------------------------------------------------------
\input{Chapters/partials/PosNovelFindsHeatmap.tex}

%----------------------------------------------------------------------------------------
%	COSMIC Set Difference as Matrix
%----------------------------------------------------------------------------------------
\input{Chapters/partials/PosDifferenceCOSMIC_Matrix.tex}

%----------------------------------------------------------------------------------------
%	COSMIC Hypergeometics Test
%----------------------------------------------------------------------------------------
\section{COSMIC Hypergeometric Testing}
Only using the COSMIC gene set to determine which finds are novel provides no value for the measuring the overall significance of finds with respect to capturing known cancer drivers. Therefore the hypergeometric test was performed using the following values: 
\begin{enumerate*}
\item the number of genes in the intersect between COSMIC and my significant set, 29; 
\item the number of genes in COSMIC, 616; 
\item the number of genes with mutations in TCGA set , 18201; and
\item the number of genes in my significant set, 384
\end{enumerate*}. This resulted in a p-value of $5.0875 \times 10^{-5}$, see \eqref{Eq-PosCOSMIC_phyper} for calculation and \eqref{Eq-phyper} for general equation. This p-value indicates the significant set determined via positional analysis has a high degree of true positives.

\begin{equation}
\label{Eq-PosCOSMIC_phyper}
p(k = 29) = {{{{616 \choose {29-1}} {{18201-616} \choose {384-29}}}} \over {{18201 \choose 384}}} = 5.0875 \times 10^{-5}
\end{equation}

Note that in \eqref{Eq-PosCOSMIC_phyper}, $k-1$ successes are considered to find the cumulative probability of $k$ or more successes.

%----------------------------------------------------------------------------------------
%	HEATMAPS
%----------------------------------------------------------------------------------------
\section{Mutational Prevalence}
%\comment{TODO: Make the genes in COSMIC bold to differentiate them in these heatmaps.}
\label{Sec-PosHeatmaps}
In order to measure the prevalence of mutations across missense-only and all-mutations profiles heatmaps were created where cells are colored by the ratio between number of patients with a mutation in the given gene over the number of patients in that cancer type.
The mutation profile naming scheme in these images is such that \textit{mut} profiles include all observed mutations in the cancer profile (i.e., synonymous and missense mutations); while \textit{missense} profiles include only missense mutations in the cancer profile (i.e., synonymous mutations have been removed).

\begin{figure}[h]
\caption[Positional Heatmap]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles.
}
\centering
\label{Fig-PosHeatmapBoth}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/PosResults/Heatmaps/both-heatmap-1.pdf}
\end{figure}


\begin{figure}[h]
\caption[Positional All Mutations Heatmap]{
\small
A heatmap showing the significant genes compared across all cancer types with only \textit{mut} background mutation profiles, or those considering all mutations, both synonymous and missense.
}
\centering
\label{Fig-PosHeatmapAllMuts}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/PosResults/Heatmaps/all-muts-heatmap-1.pdf}
\end{figure}

\begin{figure}[h]
\caption[Positional Missense Mutations Heatmap]{
\small
A heatmap showing the significant genes compared across all cancer types with only \textit{missense} background mutation profiles, or those considering only missense mutations with synonymous mutations removed.
}
\centering
\label{Fig-PosHeatmapMissense}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/PosResults/Heatmaps/missense-heatmap-1.pdf}
\end{figure}

These heatmaps show the mutation prevalence across individuals is low for most genes. As is expected, there is a high mutation prevalence in genes such as TP53, a ubiquitous tumor-suppressor gene. More important than the prevalence of mutations across genes in a cancer is comparison between cancers within the context of significant genes.
Note that cancer profiles with a high mutation prevalence for a particular gene are not necessarily targeting disordered positions within that gene, they simply have a high number of patients with mutations in the gene.
%As such, later in the results I consider both significance\comment{Implies statistical significance, which is not determined by the heatmaps so needs a rephrasing here.} by heatmap (high patient mutation prevalence) and significance by Monte Carlo simulation (high positive selection for disordered positions).

Copies of these heatmaps with only novel finds (genes not already in the COSMIC gene set) are available as \fgref{Fig-PosHeatmapBothNovel} (both mutation profiles), \fgref{Fig-PosHeatmapAllMutsNovel} (only \textit{mut} profiles), and \fgref{Fig-PosHeatmapMissenseNovel} (only \textit{missense} profiles).


%----------------------------------------------------------------------------------------
%	Chimera Plots
%----------------------------------------------------------------------------------------
\section{Visualizations of Select Genes}
For those novel finds with Protein Data Bank (PDB) entries at a resolution of  $< 2.5$ \r{A}, observed mutations were visualized using UCSF Chimera, production version 1.11.2 (build 41380) along with tables listing the observed mutations. Note that due to the inherent difficulty in generating a PDB structure for a disordered protein -- especially for so fine a resolution -- these images and results are biased toward the more ordered genes in the significant set. Images here were selected for illustrative purposes.

\input{Chapters/partials/pos-chimera-plots.tex}


%----------------------------------------------------------------------------------------
%	Enrichment Analysis
%----------------------------------------------------------------------------------------
\section{Enrichment Analysis}
There were no significant terms following FDR correction, however the top 10 terms prior to correction are listed in \tbref{Tab-PosEA_Uncorrected}.

\input{Chapters/partials/pos_nonsignificant_ea.tex}


%----------------------------------------------------------------------------------------
%	Partner Set Enrichment Analysis
%----------------------------------------------------------------------------------------
\section{Partner Set Enrichment Analysis}
Utilizing \textit{Homo sapiens} data from BioGRID downloaded from their latest release on June 14th, 2017 any direct interactors with the significant set were extracted into their own binding partner set (duplicate entries were removed). This resulted in $1545$ gene symbols, which when run through the same enrichment analysis process resulted in hundreds of enriched terms. Considering only the most specific terms by removing parents in the graph, a total of $168$ terms were enriched with the top 10 listed in \tbref{Tab-PosEA_NeighborsSpecific} (all $168$ terms can be seen in \tbref{Tab-PosFullNeighborTerms}).

\input{Chapters/partials/pos_neighbors_ea.tex}
