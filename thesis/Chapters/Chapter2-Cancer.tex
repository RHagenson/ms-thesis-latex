% Chapter on Cancer

\chapter{Cancer} % Main chapter title

\label{Ch-Cancer} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Introduction}
Cancers are driven by somatic mutations; those mutations that occur within the cells of the body not within the germ line and as a result are passed down to subsequent cellular generations via cell division.

As a disease, cancer is especially complex due to the great variety of mutations that can occur, where they occur, which tissue(s) they are affecting, the individual's personal genetics/diet/activity level, and more all having an effect in the progression of the disease. We know far more about cancer today than we ever have before due in no small part to our increasing ability to capture data on and understanding how all these pieces contribute to cancer. Although many know of certain risk factors for cancer such as over exposure to UV radiation via sunlight, there are many more subtle risk factors for cancer. 


%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Causes of Cancer}
Cancer being driven by mutations means that almost any aspect of life that causes mutations, prevents proper repair of genetic mistakes, or otherwise increases the mutations rate in a line of cells can be linked to an increased risk for developing cancer. However, there are also inhibitory effects brought on by some of these same aspects. No where is this more well-noted than in countering news reports released weeks or even days apart from one another stating that drinking coffee or any commonplace routine stating that "A recent study shows that [routine] increases/decreases the risk for cancer." These reports are often not overtly wrong, but are reductionist to the point of blurring the truth with the original report showing what is being stated in the news, but the reductionist view avoiding the underlying truth that the complex nature of oncogenesis can be effected by these activities differently in a case-by-case basis. That is not to say none of these routines are well-established in causing cancer. A strong example being the smoking of tobacco, which was epidemiologically-linked to increased risk of lung cancer a long time ago. Ultimately, cancer is driven by mutations and these occurs in two major causal classes: external to the body and internal to the body.

\subsection{Mutations from External Mutagens}
External mutagens are those which occur outside the body and affect the mutation rate within the body. Even people without a scientific background understand these mutagens quite clearly and take steps to avoid them in most cases. Often these are seen as activities/chemicals that a person is exposed to or willingly does that are linked with an increased risk for cancer. These include the aforementioned activities of smoking tobacco and exposing oneself to UV radiation via sunlight. Such activities causes chemical changes within the cell which lead to cellular mutations. Other commonly known external mutagens are radiation and chemical spills, which the fear of has limited growth of certain industries and increased environmental regulations to protect citizens from these less easily identified risk factors. The less easily identified risks are no less important that the risks that are easily avoided.


\subsection{Mutations from Internal Mutagens}
Internal mutagens are those which occur within the body to affect the mutation rate within the body. Often these are far less understood by people without a scientific background and are often understated even by people with a scientific background. One internal mutagen is the DNA repair mechanisms not being able to identify a mistake in replication. Or in more detail, as DNA is replicated prior to cellular division a copy must be made that will split off into the daughter cell following division. This replication process involves opening the DNA double helix via helicase and building two new complementary strands of DNA to both halves via DNA polymerase. The average mistake rate for DNA polymerase is one in one hundred thousand $\left(\frac{1}{100,000}\right)$. This may seem like an acceptable error rate however there are six billion ($6,000,000,000$) base pairs in a human diploid cell, which amounts to an average of sixty thousand ($60,000$) errors at each division. The cell is able to repair most, but not all, of these mistakes. The remaining mistakes are by definition mutations and these mutations have the risk of being cancer-causing. 

Another internal mutagen is the progressive shortening of telomeres with each cell division. Telomeres are repeated segments of DNA at the ends of chromosomes which protect the internal coding portions from mutations and degradation by being mutated and degraded themselves.


%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Effects of Cancer}
Without entering into the emotional or familial effects and focusing solely within the area of molecular effects of cancer, there are two names trusted with the uncovering the \textit{Hallmarks of Cancer}: Douglas Hanahan and Robert Weinberg. Together they noted the six hallmarks which are characteristic of cancer. 

\subsection{Hallmarks of Cancer}
% All information in this section is directly out of Hanahan and Weinberg's
% 2011 review
\citeauthor{Hanahan2000} noted the following six hallmarks of cancer in the \citeyear{Hanahan2000} original review: 
\begin{enumerate*}
\item Sustaining proliferative signaling, 
\item Evading growth suppressors, 
\item Activating invasion and metastasis, 
\item Enabling replicative immortality, 
\item Inducing angiogensis, 
\item Resisting cell death
\end{enumerate*}. 
In addition, the following two emerging hallmarks were noted in their \citeyear{Hanahan2011} updated review:
\begin{enumerate*}
\setcounter{enumi}{6}
\item Deregulating cellular energetics, 
\item Avoiding immune destruction
\end{enumerate*}. 
All of these are discussed in more detail below. It is important to note that not all cancers begin with all of these effects, rather linking a mutation to any of the hallmarks indicates potential involvement in oncogensis.

\subsubsection*{Sustaining proliferative signaling}
The most fundamental trait of cancer cells is their ability to continually proliferate. Unlike normal cells, cancer cells do not regulate their growth to stay within a set limit either for individual cell growth, resulting in larger cells, or the number of cellular divisions, resulting in more cells.

Investigating the normal proliferative signaling is complicated by it likely being both temporally and spatially localized -- this localization is difficult to investigate experimentally. By contrast, the proliferative signaling of cancer cells is far better understood with mechanisms such as producing growth factor themselves (autocrine proliferation) or sending signals to surrounding healthy cells, which in turn respond with growth factors \cite{Cheng2008,Bhowmick2004}, an example of the "tumor microenvironment" now being considered.

The "tumor microenvironment" or involvement of healthy cells in development and expression of cancer hallmarks is a more recent understanding within oncogensis. It is now understood that cancer cells have the ability to "recruit" healthy cells into working for them rather than working for what is best for the healthy cells or body. This quasi-parasitic hijacking of the normal cellular microenvironment is one example of why studying cancer is often not isolated to looking only at the cancer cells themselves, but rather their context as well.

For the work herein, one interesting point of this hallmark is that cancer cells also leverage binding relationships and signal amplification to display it. Disordered regions are often the complement to the otherwise rigid nature of the ordered regions in proteins. It is possible that a mutation which makes a receptor less specific for its normal ligand might be a cause for sustained proliferative signaling via being triggered by similar ligands in addition to the normal ligand. This speculative mechanism would likely be linked to a mutation within a tyrosine kinase domain, which are often found in cell-surface receptors that respond to growth factor for proliferative purposes.

It is not only accelerating mutations which occur to incite this hallmark. Some mutations occur to disrupt the negative-feedback mechanisms resulting in a loss of homeostatic control over proliferative signaling. Examples of this loss-of-function provocation include B-Raf protein \cite{Karasarides2004,Li2007,Dhillon2004}, 
phosphoinositide 3-kinase \cite{Yuan2008,Jiang2008}, 
and mTOR kinase \cite{OReilly2006,Sudarsanam2010}. Each of these share the common mechanism of inhibiting antiproliferative effects, resulting in excessive proliferation within the affected cancer cells. 


\subsubsection*{Evading growth suppressors}
Besides continually proliferating, a noted hallmark of cancer is the evasion of growth suppressors. Through inhibition of tumor suppressors cancers cells evade the usual checks to growth which limit cell size. The two most notable tumor suppressor sgenes are: pRB (retinoblastoma-associated protein) and TP53 (tumor protein p53, often just called p53). Within the cell cycle, these two act as the major control points for cellular growth; if either detects a mistake has been made or the cell is not normal, they should signal for the cell to die (apoptosis). In cancer, these two are inhibited, removing checks on cancer cell growth and proliferation. Both of these provide a check on cellular health between the G1 and S phases of the cell division cycle. Despite what might be originally thought, inhibition of pRB or p53 does not always mean uncontrolled growth. Both are part of protein functional networks that provide some level of redundancy to the cell division cycle checks.

For the work herein, there is almost a sure guarantee that p53 will be significant as it is significant in most studies of cancer. Being a tumor suppressor gene that regulates cell growth and proliferation most mutations result in a loss-of-function. Tumor suppressors being the "brakes" on a runaway cell line means any malfunction and the cell line is not prevented from running away. Even a mutation that only mildly inhibits p53's function can result in mutations within unhealthy cells accumulating. Remember that DNA polymerase makes an average of sixty thousand mistakes during each replication step, those cells with dangerous mutations would normally be prevented from dividing and destroyed via p53.


\subsubsection*{Activating invasion and metastasis}
We are progressively understanding more about the mechanisms underlying invasion and metastasis. Largely many of the genes associated with this hallmark encode for cell-to-cell and cell-to-ECM (extracellular matrix) adhesion roles. 

The process of invasion and metastasis has been termed the \textit{invasion-metastasis cascade}. A brief overview of this process is that there is:
\begin{enumerate*}
\item local invasion, 
\item intravasation into nearby blood and lymphatic vessels, 
\item transportation via the vessels to a new area of the body, 
\item extravasation into new areas of the body, 
\item formation of of micrometastases or small cancer nodules,
\item growth of micrometastases into macroscopic tumors, or "colonization"
\end{enumerate*}. 

For the work herein, linking back to this hallmark is unlikely due to the continued enigma that is invasion and metastasis. However, there might be some case to be made if a disordered region mutation could cause loss of cell-to-cell or cell-to-ECM adhesion.


\subsubsection*{Enabling replicative immortality}
It is fairly well understood that cancer cells gain replicative immortality wherein they do not enter into the crisis stage of most cell lines -- where most of the cells die and its the result of a certain number of division cycles. Immortalized cells lines show no sign of either senescence, a nonproliferative phase, or crysis, an apoptitic phase. One aspect of the genome that is essential to protecting the cell line and bringing about immortalization is the presence of protective telomeres \cite{Collado2007,Campisi2013,Harley1992,Shay2000,Blasco2005}. 

Telomeres are repeated stretches of DNA at the ends of chromosomes that protect the internal coding segments from mutation and erosion during DNA replication. They are lengthened via telomerase, directly countering the telomeric erosion that occurs with each cell division. 

For the work herein, it is not likely any results will be linked to this hallmark. Primary function of telomerase is highly specific and does not involve binding relationships with other proteins, rather it binds with DNA. However, there is some evidence showing telomerase acts as a cofactor in the $\beta$-catenin/LEF transcription factor complex \cite{Park2009}, has involvement in DNA-damage repair \cite{Masutomi2005}; and its protein subunit, TERT, is found associated with chromatin in regions besides telomeres \cite{Park2009,Masutomi2005}.


\subsubsection*{Inducing angiogensis}
Angiogenesis is the process of forming new blood vessels. Tumors, just like normal tissue, require a means to receive nutrients and oxygen while expelling metabolic waste and carbon dioxide. Thus it is a hallmark of cancer to induce angiogenesis to support the increased cell size and cell quantity characteristic of a tumor.

The most well-known associated genes are vascular endothelial growth factor-A (VEGF-A, an angiogenesis inducer) and thrombospondin-1 (TSP-1, an angiogenesis inhibitor). For the work herein, mutations within fibrous proteins could very well link back to this hallmark since the vascular walls are largely connective tissue.


\subsubsection*{Resisting cell death}
Apoptosis or programmed cell death is incited within cells that have a high degree of genome damage via accumulated mutations in addition to other less cancer-driving reasons. The mechanism for apoptosis is comprised of two major parts: upstream regulators and downstream effectors. Both of these are essential for apoptosis to occurs properly. Once the cell dies, its cellular material is consumed by its neighbors and by phagocytic cells to recycle the material. 

Apoptosis is a balance between anti- and proapoptitic memembers of the Bcl-2 family of proteins along with similar proteins (Bcl-x\textsubscript{L}, Bcl-w, Mcl-1, A1); all of these act by binding with two proapoptitic proteins (Bax and Bak). This inherent binding relationship brings about a strong point for the work herein that mutations within disordered regions on either side of this binding partnership might causes the apoptitic mechanisms/pathways to shift in function either via gain-of-function or loss-of-function. 


\subsubsection*{Deregulating cellular energetics}
% Called "An Emerging Hallmark: Reprogramming Energy Metabolism" in paper
An emerging hallmark is the deregulation of cellular energetics to support the unchecked cell proliferation and growth. Observed very early on by \citeauthor{Warburg1925}, cancer cells tend to switch over to a state termed "aerobic glycolysis" which is less energy efficient, but does not utilize mitochondria. This lead to the Warburg hypothesis that the driver of tumorigenesis is an insufficient cellular respiration caused by insult/injury to mitochondria \cite{Warburg1925,Warburg1956b,Warburg1956}. The decrease in energy efficiency is overcome in large part by cancer cells up-regulating glucose transporters such as GLUT1 -- increasing the import of glucose and thereby relieving the deficit \cite{DeBerardinis2008,Jones2009,Hsu2008}.

Although fully rationalizing why cancer cells preferentially utilize aerobic glycolysis rather than the more efficient oxidative phosphorylation has been difficult, one possible explanation is that by using glycolysis the intermediates can be used in various biosynthetic pathways. This idea is supported by the same Warburg-like metabolism being noted in rapidly dividing embryonic tissues, suggesting there is some advantage to this switch in cell lines of rapid proliferation.


\subsubsection*{Avoiding immune destruction}
Our immune system should detect and destroy incipient cancers, however this is not the case therefore one hallmark must be that cancer avoids destruction by the immune system. This is supported not only by the reasoning of the problem, but also by immunocompromised individuals having an increase occurrence of certain cancers \cite{Vajdic2009}. From mice knockout studies, we know that deficiencies in in CD8$^+$ cytotoxic T lymphocytes (CTLs), CD4$^+$ T$_h$1 helper T cells, or natural killer (NK) cells, all lead to increased tumorigenesis. As well, transplanting cancer cells from immunodeficient mice into immunocompetent mice had a decreased rate of secondary tumors, whereas cancer cells from immunocompetent mice had equal rates of secondary tumors in both case and control mice.

For the work herein it is likely to trace an observation to this emerging hallmark, however the data used is unlikely to provide conclusive evidence to link the observation and hallmark -- wet lab validation would be needed to test the validity of any observations.


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\section{The Cancer Genome Atlas}
The Cancer Genome Atlas (TCGA) was the leading effort to catalog genetic mutations in cancer via high-throughput genomics -- bettering our understanding of the genetic basis of cancer with a primary goal of improving diagnosis, treatment, and prevention of cancer. 
According to The Cancer Genomes Atlas's About page: "The Cancer Genome Atlas (TCGA) is a collaboration between the National Cancer Institute (NCI) and the National Human Genome Research Institute (NHGRI) that has generated comprehensive, multi-dimensional maps of the key genomic changes in 33 types of cancer."\footnote{\url{https://cancergenome.nih.gov/abouttcga}}
Over its lifespan from 2005 to 2017, it collected 2.5 petabytes of data from more than 11,000 patients describing the mutational load of 33 cancer types. The data used in this study is from July 18th, 2016 -- thus is not the final state of TCGA with ongoing investigation in the following Analysis Working Groups (AWGs): 
\begin{enumerate*}
\item Liver hepatocellular carcinoma,
\item Mesothelioma,
\item Pancreatic ductal adenocarcinoma,
\item Sarcoma,
\item Testicular germ cell cancer,
\item Thymoma,
\item Uveal melanoma
\end{enumerate*}.

\subsection{Methods}
The TCGA Research Network consists of many parts; each part is integral to achieving TCGA's central goal. Beginning with the Biospecimen Core Resource (BCR), which reviews and processes the initial blood and tissue sample, to the Analysis Working Groups (AWGs), which are made up of scientific and clinical experts, analyze a single type of cancer across all TCGA methods and publish a comprehensive analysis of findings. See \tbref{Tab-TCGA_Network} for more details on the TCGA Research Network.

Seven facets of genomic data were captured for each pair of samples (tumor and control tissue); these are listed in \tbref{Tab-TCGA_Platforms}.



\subsection{Cancers in the Atlas}
Under TCGA investigation there were 33 tumor types (see \tbref{Tab-TCGA_Selected_Cancers}), which fit into 31 cancer type categories (see \tbref{Tab-TCGA_Cancers}). These include 10 rare cancer listed in \tbref{Tab-TCGA_RareCancers} -- one of which, mesothelioma, was not under analysis. See \tbref{Tab-TCGA_Cancers} for more a complete listing of the major cancer types included in this study.


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\subsection{Data Acquisition}
Cancer mutation data was obtained from the latest available TCGA\footnote{\url{https://cancergenome.nih.gov/}} run on July 18th, 2016 from the Broad Institute Firehose system\footnote{\url{http://firebrowse.org}}, in the form of Data Level 2 (\textit{Processed Data}), which is the level of consensus results from processing the raw genome sequencing reads (Data Level 1 \textit{Raw Data}). This data includes 31 cancer types, listed in \tbref{Tab-TCGA_Cancers}.

% Taken out because it is bulk that is not needed
% and marks nearly the final state of the initiative since it will officially close in early 2017.\footnote{\url{https://cancergenome.nih.gov/abouttcga/overview}}

\subsection{Dataset Size}
The TCGA dataset used contained information on $95,836$ proteins, each of which was processed twice positional and once regional. Positional analysis was done on measurements by IUPred "long", or a $100$ residue interaction window, and IUPred "short", or a $25$ residue interaction window \cite{Doszt??nyi2005}; regional analysis was done on measurements by FoldIndex \cite{Prilusky2005}. Calculations from both positional measurements are based on pairwise chemical interaction energies and smoothed over a window size of 21 residues -- this is in accordance with the IUPred method \cite{Dosztanyi2005a}. These long and short measurements are processed concurrently, but separately at each step. Calculations from regional measures are based on measures of hydrophobicity and net charge and are not smoothed over a window -- this is in accordance with the FoldIndex method \cite{Prilusky2005}.
