% Chapter Template

\chapter{Regional Analysis Results} % Main chapter title

\label{Ch-Regional} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
% Introduction
%----------------------------------------------------------------------------------------
\section{Introduction}
Binomial testing results across 31 cancer types (listed in \tbref{Tab-TCGA_Cancers}) were limited only by the parameters stated in \scref{Sec-BinomialTesting}. These parameters are already highly-conservative while still resulting in $1819$ significant genes. Well-characterized driver genes were removed by taking the set difference between the COSMIC gene set and this initial significant set. This left $623$ remaining gene symbols across both missense-only and all-mutations profiles. See \fgref{Fig-RegFindsHeatmap-1} through \fgref{Fig-RegFindsHeatmap-8} for a binary mapping of these $623$ gene symbols to which cancer backgrounds they were significant within and \tbref{Tab-RegNovelFinds} for a table of the significant gene symbols.


%----------------------------------------------------------------------------------------
%	COSMIC Set Difference as Matrix
%----------------------------------------------------------------------------------------
\input{Chapters/partials/RegNovelFindsHeatmap.tex}

%----------------------------------------------------------------------------------------
%	COSMIC Set Difference as Matrix
%----------------------------------------------------------------------------------------
\input{Chapters/partials/RegDifferenceCOSMIC_Matrix.tex}

%----------------------------------------------------------------------------------------
%	COSMIC Hypergeometics Test
%----------------------------------------------------------------------------------------
\section{COSMIC Hypergeometric Testing}
Only using the COSMIC gene set to determine which finds are novel provides no value for the measuring the overall significance of finds with respect to capturing known cancer drivers. Therefore the hypergeometric test was performed using the following values: 
\begin{enumerate*}
\item the number of genes in the intersect between COSMIC and my significant set, 44; 
\item the number of genes in COSMIC, 616; 
\item the number of genes with mutations in TCGA set , 18201; and
\item the number of genes in my significant set, 1819
\end{enumerate*}. This resulted in a p-value of $0.9988$, see \eqref{Eq-RegCOSMIC_phyper} for calculation and \eqref{Eq-phyper} for general equation. This p-value indicates regional analysis has a high degree of false positives.

\begin{equation}
\label{Eq-RegCOSMIC_phyper}
p(k = 44) = {{{{616 \choose {44-1}} {{18201-616} \choose {1819-44}}}} \over {{18201 \choose 1819}}} = 0.9988
\end{equation}

Note that in \eqref{Eq-RegCOSMIC_phyper}, $k-1$ successes are considered to find the cumulative probability of $k$ or more successes. This value would likely become $1$ if all significant results were analyzed rather than limiting to the top $100$ results in each cancer background as stated in \scref{Sec-BinomialTesting}.

%----------------------------------------------------------------------------------------
% Heatmaps
%----------------------------------------------------------------------------------------
\section{Mutational Prevalence}
\label{Sec-RegHeatmaps}
In order to measure the prevalence of mutations across missense-only and all-mutations profiles heatmaps were created where cells are colored by the ratio between number of patients with a mutation in the given gene over the number of patients in that cancer type.
The mutation profile naming scheme is such that \textit{mut} profiles include all observed mutations in the cancer profile (i.e., synonymous and missense mutations); while \textit{missense} profiles include only missense mutations in the cancer profile (i.e., synonymous mutations have been removed). Only novel finds (genes not found in the COSMIC gene set) are considered in these heatmaps and the same scale is used for each heatmap in a set.

These heatmaps show the mutation prevalence across individuals is low for most genes. As is expected, there is a high mutation prevalence in genes such as TP53, a ubiquitous tumor-suppressor gene. More important than the prevalence of mutations across genes in a cancer is comparison between cancers within the context of significant genes. 
Note that cancer profiles with a high mutation prevalence for a particular gene are not necessarily targeting disordered positions within that gene, they simply have a high number of patients with mutations in the gene.

\subsection{Both Profiles}
The heatmaps are arranged by significant gene in alphabetical order, with a single gene overlap between each, therefore:
\begin{enumerate}
\item ABRA through FAM71E2 can be seen in \fgref{Fig-RegHeatmapBoth-1}.
\item FAM71E2 through MYH4 can be seen in \fgref{Fig-RegHeatmapBoth-2}
\item MYH4 through RBMXL3 can be seen in \fgref{Fig-RegHeatmapBoth-3}
\item RBMXL3 through TRAPPC12 can be seen in \fgref{Fig-RegHeatmapBoth-4}
\item TRAPPC12 through ZFP64 can be seen in \fgref{Fig-RegHeatmapBoth-5}
\item ZFP64 through ZNF334 can be seen in \fgref{Fig-RegHeatmapBoth-6}
\item ZNF334 through ZNF610 can be seen in \fgref{Fig-RegHeatmapBoth-7}
\item ZNF610 through ZWINT can be seen in \fgref{Fig-RegHeatmapBoth-8}
\end{enumerate}


\begin{figure}[!ht]
\caption[Regional Heatmap 1]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (1 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-1}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-1.pdf}
\end{figure}

\begin{figure}[!ht]
\caption[Regional Heatmap 2]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (2 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-2}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-2.pdf}
\end{figure}

\begin{figure}[!ht]
\caption[Regional Heatmap 3]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (3 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-3}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-3.pdf}
\end{figure}

\begin{figure}[!ht]
\caption[Regional Heatmap 4]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (4 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-4}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-4.pdf}
\end{figure}

\begin{figure}[!ht]
\caption[Regional Heatmap 5]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (5 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-5}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-5.pdf}
\end{figure}

\begin{figure}[!ht]
\caption[Regional Heatmap 6]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (6 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-6}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-6.pdf}
\end{figure}

\begin{figure}[!ht]
\caption[Regional Heatmap 7]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (7 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-7}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-7.pdf}
\end{figure}

\begin{figure}[!ht]
\caption[Regional Heatmap 8]{
\small
A heatmap showing the significant genes compared across all cancer types with both background mutation profiles. (8 of 8)
}
\centering
\label{Fig-RegHeatmapBoth-8}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-both-8.pdf}
\end{figure}


%\subsection{All Mutations Profiles}
%The heatmaps are arranged by significant gene in alphabetical order, with a single gene overlap between each, therefore:
%\begin{enumerate}
%\item ABRA through FYB can be seen in \fgref{Fig-RegHeatmapMuts-1}.
%\item FYB through MYH2 can be seen in \fgref{Fig-RegHeatmapMuts-2}
%\item MYH2 through RINL can be seen in \fgref{Fig-RegHeatmapMuts-3}
%\item RINL through VCX3B can be seen in \fgref{Fig-RegHeatmapMuts-4}
%\item VCX3B through ZWINT can be seen in \fgref{Fig-RegHeatmapMuts-5}
%\end{enumerate}
%
%\begin{figure}[!ht]
%\caption[Regional All Mutations Heatmap 1]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{mut} background mutation profiles, or those considering all mutations, both synonymous and missense. (1 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMuts-1}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-all-muts-1.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional All Mutations Heatmap 2]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{mut} background mutation profiles, or those considering all mutations, both synonymous and missense. (2 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMuts-2}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-all-muts-2.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional All Mutations Heatmap 3]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{mut} background mutation profiles, or those considering all mutations, both synonymous and missense. (3 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMuts-3}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-all-muts-3.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional All Mutations Heatmap 4]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{mut} background mutation profiles, or those considering all mutations, both synonymous and missense. (4 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMuts-4}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-all-muts-4.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional All Mutations Heatmap 5]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{mut} background mutation profiles, or those considering all mutations, both synonymous and missense. (5 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMuts-5}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-all-muts-5.pdf}
%\end{figure}


%\subsection{Missense-only Mutations Profiles}
%The heatmaps are arranged by significant gene in alphabetical order, with a single gene overlap between each, therefore:
%\begin{enumerate}
%\item ABRA through FYB can be seen in \fgref{Fig-RegHeatmapMissense-1}.
%\item FYB through MYH2 can be seen in \fgref{Fig-RegHeatmapMissense-2}
%\item MYH2 through RINL can be seen in \fgref{Fig-RegHeatmapMissense-3}
%\item RINL through VCX3B can be seen in \fgref{Fig-RegHeatmapMissense-4}
%\item VCX3B through ZWINT can be seen in \fgref{Fig-RegHeatmapMissense-5}
%\end{enumerate}
%
%\begin{figure}[!ht]
%\caption[Regional Missense Mutations Heatmap 1]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{missense} background mutation profiles, or those considering only missense mutations with synonymous mutations removed. (1 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMissense-1}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-missense-only-1.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional Missense Mutations Heatmap 2]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{missense} background mutation profiles, or those considering only missense mutations with synonymous mutations removed. (2 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMissense-2}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-missense-only-2.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional Missense Mutations Heatmap 3]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{missense} background mutation profiles, or those considering only missense mutations with synonymous mutations removed. (3 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMissense-3}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-missense-only-3.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional Missense Mutations Heatmap 4]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{missense} background mutation profiles, or those considering only missense mutations with synonymous mutations removed. (4 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMissense-4}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-missense-only-4.pdf}
%\end{figure}
%
%\begin{figure}[!ht]
%\caption[Regional Missense Mutations Heatmap 5]{
%\small
%A heatmap showing the significant genes compared across all cancer types with only \textit{missense} background mutation profiles, or those considering only missense mutations with synonymous mutations removed. (5 of 5)
%}
%\centering
%\label{Fig-RegHeatmapMissense-5}
%\includegraphics[width=\linewidth,
%				 height=\textheight,
%				 keepaspectratio]
%				 {./imgs/RegResults/Heatmaps/plot-split-heatmap-missense-only-5.pdf}
%\end{figure}

\subsection{Mutation Prevalence Distributions}
Given the many rows and necessary splitting of these results across many figures, in order to facilitate better understanding of the cancer-wise (\fgref{Fg-RegByCancerHeatmapSum}) and gene-wise (\fgref{Fg-RegByGeneHeatmapSum}) mean summaries are provided. The tables of these values are available in \apref{Ap-RegResults}.


\subsubsection{By Cancer}
\begin{figure}[!ht]
\caption[Regional Heatmap Distribution By Cancer]{
\small
The mean mutation prevalence from regional heatmaps by cancer. Note that the number of missense-only profiles (\textit{Missense} plus the number of all-mutations profiles (\textit{Muts}) is equal to the number of both profiles (\textit{Both}). This is due to \textit{Missense} and \textit{Muts} profiles being mutually exclusive.
}
\centering
\label{Fg-RegByCancerHeatmapSum}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/By-Cancer-Summary-Distribution.pdf}
\end{figure}


\subsubsection{By Gene}
\begin{figure}[!ht]
\caption[Regional Heatmap Distribution By Gene]{
\small
The mean mutation prevalence from regional heatmaps by gene. Note that here the number of missense-only profile genes (\textit{Missense} plus the number of all mutation profiles genes (\textit{Muts}) is note equal to the number of both profiles (\textit{Both}). This is due to only considering genes significant in each of the background profiles, which are not mutually exclusive. Therefore, \textit{Both} is not simply the combination of \textit{Missense} and \textit{Muts}, but rather their
union with \textit{Missense} and \textit{Muts} profiles sharing 245 entries. Lengths: \textit{Both} (623), \textit{Muts} (401), \textit{Missense} (331).
}
\centering
\label{Fg-RegByGeneHeatmapSum}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/Heatmaps/By-Gene-Summary-Distribution.pdf}
\end{figure}


%----------------------------------------------------------------------------------------
% Visualizations of Select Genes
%----------------------------------------------------------------------------------------
\section{Visualizations of Select Genes}
The three genes selected here are for illustrative purposes of the result of regional analysis. They were chosen due to being within the top five significant results of their cancer background and being among greatest 20 absolute observed disorder loads across all results -- balancing the positional disorder and number of mutations observed in the gene. There were no available Protein Data Bank (PDB) structures for these genes, which might suggest they are entirely or partially too disordered to proper crystallize as is necessary to create the PDB structures.

\subsection{HRC.001 in ACC}
\subsubsection{Smoothed Disorder Plot with Mutations}
In \fgref{Fg-Reg_HRC.001}, the mutations from ACC\_mut (Adrenocortical carcinoma, all mutations in background) are mapped onto a smoothed disorder plot. All mutations occur at the most disordered region of the protein with the observed position-wise disorder score among these mutated positions being $\approx -0.75$ -- well below the $-0.1$ threshold used to annotate high-confidence disordered regions.

\begin{table}
\centering
\caption[ACC HRC Mutations]{The mutations noted in ACC\_mut for HRC in the TCGA dataset.
}
\label{Tab-ACC_HRC_Mutations}
\begin{small}
\begin{tabular}{|l|l|l|} \hline
% Header
\textbf{Isoform} & 
\textbf{Amino Acid Position} & 
\textbf{Frequency} \nl
% End header
HRC.001 & 193 & 2 \nl
HRC.001 & 202 & 1 \nl
HRC.001 & 247 & 1 \nl
HRC.001 & 48 & 4 \nl
\end{tabular}
\end{small}
\end{table}

\begin{figure}[!ht]
\caption[Smoothed HRC.001 Disorder]{
\small
The smoothed disorder scores of HRC.001 in ACC\_mut. The red dots are mutated positions and exact disorder score at that position.
}
\centering
\label{Fg-Reg_HRC.001}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/DisorderPlots/HRC_001.pdf}
\end{figure}

Note that this isoform, HRC.001, was also significant within:
\begin{description}
\item[BLCA\_mut] Bladder urothelial carcinoma, all mutations in background,
\item[CESC\_mut] Cervical squamous cell carcinoma and endocervical adenocarcinoma, all mutations in background
\item[HNSC\_mut] Head and neck squamous cell carcinoma, all mutations in background
\item[LUAD\_mut] Lung adenocarcinoma, all mutations in background
\item[TGCT\_mut] Testicular germ cell tumors, all mutations in background
\item[CESC\_missense] Cervical squamous cell carcinoma and endocervical adenocarcinoma, missense-only mutations in background
\end{description}

\subsection{IVL.001 in ESCA}
\subsubsection{Smoothed Disorder Plot with Mutations}
In \fgref{Fg-Reg_IVL.001}, the mutations from ESCA\_mut (Esophageal carcinoma, all mutations in background) are mapped onto a smoothed disorder plot.
All mutations occur below the high confidence threshold ($-0.1$) for determining a disordered region. The mutations occur across the protein affecting mostly the intermediate positions, which are more disordered than the termini. This observation is opposed to the general trend of the disorder in a protein being greatest at the termini (in this case the most negative) and the intermediate positions being more ordered.

\begin{table}
\centering
\caption[IVL ESCA Mutations]{The mutations noted in ESCA\_mut for IVL in the TCGA dataset.
}
\label{Tab-ESCA_IVL_Mutations}
\begin{small}
\begin{tabular}{|l|l|l|} \hline
% Header
\textbf{Isoform} & 
\textbf{Amino Acid Position} &
\textbf{Frequency} \nl
% End header
IVL.001 & 78 & 1 \nl
IVL.001 & 173 & 1 \nl
IVL.001 & 176 & 1 \nl
IVL.001 & 180 & 1 \nl
IVL.001 & 182 & 1 \nl
IVL.001 & 202 & 1 \nl
IVL.001 & 228 & 1 \nl
IVL.001 & 252 & 1 \nl
IVL.001 & 272 & 1 \nl
IVL.001 & 282 & 1 \nl
IVL.001 & 283 & 1 \nl
IVL.001 & 332 & 1 \nl
IVL.001 & 333 & 1 \nl
IVL.001 & 339 & 1 \nl
IVL.001 & 439 & 1 \nl
\end{tabular}
\end{small}
\end{table}

\begin{figure}[!ht]
\caption[Smoothed IVL.001 Disorder]{
\small
The smoothed disorder scores of IVL.001 in ESCA\_mut. The red dots are mutated positions and exact disorder score at that position.
}
\centering
\label{Fg-Reg_IVL.001}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/DisorderPlots/IVL_001.pdf}
\end{figure}

Note that this isoform, IVL.001, was also significant within: 
\begin{description}
\item[ESCA\_missense] Esophageal carcinoma, missense-only mutations in background
\end{description}

\subsection{NEFH.001 in ACC}
\subsubsection{Smoothed Disorder Plot with Mutations}
In \fgref{Fg-Reg_NEFH.001}, the mutations from ACC\_mut (Adrenocortical carcinoma, all mutations in background) are mapped onto a smoothed disorder plot.
All mutations occur below the high confidence threshold ($-0.1$) for determining a disordered region. The mutations are concentrated around an effective plateau of disorder -- suggesting this region is consistent in itself. Rather than simply being a transition region between the relative order before this region and relative disorder after this region, the plateau suggests the region maintains a given level of disorder, which might confer a given function to this region beyond simple transition between other key regions of the folded protein.

\begin{table}
\centering
\caption[NEFH ACC Mutations]{The mutations noted in ACC\_mut for NEFH in the TCGA dataset.
}
\label{Tab-KIRP_NEFH_Mutations}
\begin{small}
\begin{tabular}{|l|l|l|} \hline
% Header
\textbf{Isoform} & 
\textbf{Amino Acid Position} & 
\textbf{Frequency} \nl
% End header
NEFH.001 & 645 & 2  \nl
NEFH.001 & 646 & 1  \nl
NEFH.001 & 655 & 11 \nl
NEFH.001 & 698 & 2  \nl
NEFH.001 & 701 & 2  \nl
NEFH.001 & 702 & 2  \nl
NEFH.001 & 744 & 1  \nl
NEFH.001 & 805 & 1  \nl
\end{tabular}
\end{small}
\end{table}

\begin{figure}[!ht]
\caption[Smoothed NEFH.001 Disorder]{
\small
The smoothed disorder scores of NEFH.001 in ACC\_mut. The red dots are mutated positions and exact disorder score at that position.
}
\centering
\label{Fg-Reg_NEFH.001}
\includegraphics[width=\linewidth,
				 height=\textheight,
				 keepaspectratio]
				 {./imgs/RegResults/DisorderPlots/NEFH_001.pdf}
\end{figure}

Note that this isoform, NEFH.001, was also significant within:
\begin{description}
\item[BRCA\_mut] Breast invasive carcinoma, all mutations in background
\item[KIRP\_mut] Kidney renal papillary cell carcinoma, all mutations in background
\item[KIRP\_missense] Kidney renal papillary cell carcinoma, missense-only mutations in background
\end{description}

%----------------------------------------------------------------------------------------
% Enrichment Analysis
%----------------------------------------------------------------------------------------
\section{Enrichment Analysis}
There were $28$ significant terms following FDR correction, the top 10 of which are listed in \tbref{Tab-RegEA_Corrected}. All of these terms have to do with regulatory roles in the cell. See \tbref{Tab-RegFullEA} for full listing. When considering only the most specific terms by removing all parents only GO:0030049 "muscle filament sliding" remains. 

\input{Chapters/partials/reg_significant_ea.tex}


%----------------------------------------------------------------------------------------
%	Partner Set Enrichment Analysis
%----------------------------------------------------------------------------------------
\section{Partner Set Enrichment Analysis}
Utilizing \textit{Homo sapiens} data from BioGRID downloaded from their latest release on June 14th, 2017 any direct interactors with the significant set were extracted into their own interaction partner set (duplicate entries were removed). This resulted in $623$ gene symbols, which when run through the same enrichment analysis process as before resulted in hundreds of enriched terms. Considering only the most specific terms
by removing parents in the graph, a total of $147$ terms were enriched with the top 10 listed in \tbref{Tab-RegEA_NeighborsSpecific} (all $147$ terms can be seen in \tbref{Tab-RegFullNeighborTerms}).

%The terms listed in \tbref{Tab-RegEA_NeighborsSpecific} are similar to those found in \ref{Tab-PosEA_NeighborsSpecific}.

\input{Chapters/partials/reg_neighbors_ea.tex}
