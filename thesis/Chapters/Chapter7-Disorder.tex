% Chapter Template

\chapter{Protein Disorder} % Main chapter title

\label{Ch-Disorder} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}


%----------------------------------------------------------------------------------------
%%	SECTION 1
%%----------------------------------------------------------------------------------------
\section{Introduction}
% What is protein disorder?
Protein order/disorder is the measure of how well-defined the 3D conformational location of a given residue within the final folded protein is. An ordered region is one that adopts a well-defined 3D conformation, while a disordered region may adopt no apparent structure or many similar structures according to cellular conditions. Protein regions are made up of discrete residues each with their own order/disorder. Each residue can have as many potential inter-residue interactions as there are other residues in the protein. The combination of such interactions is what leads to the native, or biologically-functional, 3D conformation of a protein. One way of measuring the disorder of a protein is to consider each potential pairwise interaction across the length of the protein. In a protein of only $100$ amino acids in length, there are $100 \choose 2$ or $4950$ possible pairwise interactions -- a number that grows quickly with $200 \choose 2$ resulting in $19,900$ pairwise interactions. Realistically, most residues do not interact with most other residues therefore not all combinations must be considered -- in fact the na\"{i}ve method of considering all possible combinations leads to inaccurate measures of order by neglecting proximity entirely -- thus measures of disorder commonly use sliding windows to consider interactions only within a certain sequence proximity range.

Due to our knowledge that protein folding is driven by the burial of hydrophobic side chains and reduction of surface area (see \scref{Sec-ProteinFolding} for more detail), we can predict the final folded tertiary structure \textit{in silico} based on known properties of each individual amino acid in the primary sequence. These predictions approximate protein disorder by assigning a value to how predictable each residue's position is in the final structure. The two approaches used herein to predict the protein disorder from the primary sequence are:
\begin{enumerate*}
\item pairwise amino acid interactions, and
\item hydrophobicity and net charge
\end{enumerate*}. Both of these are based in the favorability of amino acid interactions to predict how the primary sequence will form secondary structures and final tertiary structure.
%In short, both of these approaches ... if there is a long stretch of hydrophobic (non-charged) amino acids we know these are going to aggregate in an aqueous environment (i.e. favorable pairwise interactions with one another) and drive a wedge between interacting hydrophilic (polar) amino acids.


%A more robust method developed by \textcite{Dosztanyi2005a}, originally designed to discriminate intrinsically unstructured proteins (IUPs) from globular proteins (well-structured proteins), uses a window size to determine how many positions to consider pairwise interactions with and uses a summation of all interaction energies to represent the effect interactions have on a given position's disorder. Although not originally designed for such use, a slight iteration on this method called IUPred is quite effective at distinguishing disordered regions from ordered regions \cite{Doszt??nyi2005}.

%Protein order/disorder is easier to visualize if we related it to something on the macroscopic level. Consider the problem of headphones that have tied themselves into knots within one's pocket -- the knotted headphones are our protein. The knots will only release, or move at all in some cases, if we forcefully manipulate them, hence these are \textit{ordered regions} because we are certain about their structure. Meanwhile, the ear-buds or headphone jack are typically left loose and swing freely even when the rest of the cord is tied into knots, hence these are \textit{disordered regions} because we are uncertain about their structure.

%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Pairwise Amino Acid Interactions}
The chemical natures of different amino acids generate either attractive (favorable) or repulsive (unfavorable) pairwise interactions. Two polar amino acids of opposite charge or two non-polar amino acids will have favorable interactions, while two polar amino acids with the same charge or a polar and non-polar amino acid pair will have unfavorable interactions. The IUPred method \cite{Dosztanyi2005a} used herein to measure positional disorder scores is based on the ENERGI method of determining pairwise amino acids interaction energy-like quantities created by \textcite{Thomas1996}. These energy-like quantities  were disseminated via iterative decoy models until they agreed with known protein folding.

Using pairwise interaction energies in this way allows each position within a protein sequence to be given a score that corresponds to how well we can predict the final 3D conformational location of that position. The IUPred method uses a scale from 0 to 1 with precision to the ten-thousandth decimal place where 0 is complete order and 1 is complete disorder \cite{Doszt??nyi2005}. This method of positional score determination was chosen for its ability to distinguish partially disordered proteins from fully disordered proteins and is currently the best method for measuring positional disorder outperforming DISOPRED2 \cite{Ward2004} and VL3-H \cite{Obradovic2003}.


%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Hydrophobicity and Net Charge}
With the most major driving force behind protein folding being the entropic penalty that forces the burial of hydrophobic amino acids, measures of hydrophobicity and net charge (an effective estimator of hydrophilicity) provide strong correlation with the final folded structure. A region of highly hydrophobic amino acids indicates the region will likely be membrane-bound and thus more likely to be ordered, while a mixed region (alternating hydrophobic residues and hydrophilic residues) is unlikely to be unbound and thus more likely to be disordered. FoldIndex, a method by \textcite{Prilusky2005}, uses an algorithm by \textcite{Uversky2000} to define a boundary line between regions of folded order and regions of unfolded disorder. Values from this method are bound between -1 and 1 with positive values begin likely folded (ordered) regions and negative values being likely unfolded (disordered) regions.