% Chapter Template

\chapter{Methodology} % Main chapter title

\label{Ch-Methods} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Introduction}
Discovery of driver genes by focusing specifically on regions with a particular biological property is a fairly standard approach. In fact, computational approaches to driver gene discovery all but require a measurable property and a biological basis for how that property can induce cancer. Past methods have considered: 
\begin{enumerate*}
\item somatic copy-number alternations (SCNAs) as is the case with GISTIC \cite{Mermel2011}, 
\item protein-coding region length, variations in mutation types, and multiple mutations in one gene as is the case with DrGaP \cite{Hua2013}, and
\item signals of positive selection as is the case with MuSiC \cite{Dees2012}, OncodriveFM \cite{Gonzalez-Perez2012}, OncodriveCLUST \cite{Tamborero2013a}, and E-Driver \cite{Porta-Pardo2014a}.
\end{enumerate*}. Critically, the positive-selection methods (which this work is considered) face the same computational challenge of differentiating signal from noise in order to draw their conclusions.


%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------
\section{Signal Versus Noise}
Differentiating signal from noise is a problem in more than just bioinformatics with importance in any field where random observations are able to mask important observations \cite{Liu2016b,Edwards1998}. There are many complex methods, such as the Fourier transform \cite{Fourier1822}, that allow making relative sense of seemingly random input, however within driver genes discovery typically the background-anomaly approach is used in conjunction with a biological property \cite{Kamburov2015,Tamborero2013a,Tamborero2013c,Gonzalez-Perez2012}. Establishing a background rate or level for a biological property allows one to begin differentiating signal from noise via deviations from this background. 

The work herein is a focal shift from past driver genes discovery methods by focusing on the under-investigated property of protein disorder. By focusing on this property in particular, it is expected to find results not found in other methods due to characterizing proteins differently than before.
To do this two approaches were taken, positional analysis via Monte Carlo simulations and regional analysis via binomial testing, both over data from The Cancer Genome Atlas (TCGA).


%----------------------------------------------------------------------------------------
%	Data Preparation
%----------------------------------------------------------------------------------------
\section{Data Preparation}
Raw TCGA data were processed following the same procedure as in \textcite{Ghersi2014}. In short, the chromosomal coordinates provided by TCGA were mapped to their protein sequence positions by using the Human Genome Reference (GRCh37.p10). The head of a sample input after this procedure can be seen in \tbref{Tab-SampleInput} -- this is the file \code{ACC\_mut.txt}, representing the background profile of Adrenocortical carcinoma considering all mutations (both missense and synonymous mutations).

\begin{table}
\centering
\caption[Sample Processed Input]{The heading 10 rows of \code{ACC\_mut.txt}. This format represents the effective input to analysis herein following the mapping of raw TCGA chromosomal coordinates to protein sequence positions.
}
\label{Tab-SampleInput}
\begin{scriptsize}
\begin{tabular}{|l|l|l|l|l|l|l|l|} \hline
% Header
\multicolumn{1}{|l|}{\bfseries Isoform} & 
\multicolumn{1}{|l|}{\bfseries TCGA Barcode} &
\multicolumn{1}{|>{\raggedright}p{0.10\textwidth}|}{\bfseries DNA Position} &
\multicolumn{1}{|>{\raggedright}p{0.05\textwidth}|}{\bfseries DNA Start} &
\multicolumn{1}{|>{\raggedright}p{0.05\textwidth}|}{\bfseries DNA End} &
\multicolumn{1}{|>{\raggedright}p{0.10\textwidth}|}{\bfseries Protein Position} &
\multicolumn{1}{|>{\raggedright}p{0.06\textwidth}|}{\bfseries Protein Start} &
\multicolumn{1}{|>{\raggedright}p{0.06\textwidth}|}{\bfseries Protein End} \nl
% End header
A1BG.001 & TCGA-OR-A5KB-01A	 & 281 & G & A & 94 & R & H \nl
A1CF.001 & TCGA-OR-A5KB-01A	 & 1167 & C & A & 389 & G & G \nl
A1CF.002 & TCGA-OR-A5KB-01A	 & 1191 & C & A & 397 & G & G \nl
A1CF.003 & TCGA-OR-A5KB-01A	 & 1191 & C & A & 397 & G & G \nl
A1CF.004 & TCGA-OR-A5KB-01A	 & 1167 & C & A & 389 & G & G \nl
A1CF.005 & TCGA-OR-A5KB-01A	 & 1215 & C & A & 405 & G & G \nl
A1CF.006 & TCGA-OR-A5KB-01A	 & 1191 & C & A & 397 & G & G \nl
A4GALT.001 & TCGA-OR-A5JY-01A & 903 & C & G & 301 & P & P \nl
AACS.001 & TCGA-OR-A5LD-01A	 & 306 & A & C & 102 & A & A \nl
AACS.001 & TCGA-PK-A5HB-01A	 & 103 & G & C & 35 & A & P \nl
\end{tabular}
\end{scriptsize}
\end{table}

\subsection{Data Acquisition}
Cancer mutation data were obtained from the latest available TCGA\footnote{\url{https://cancergenome.nih.gov/}} run on July 18th, 2016 from the Broad Institute Firehose system,\footnote{\url{http://firebrowse.org}} in the form of Data Level 2 (\textit{Processed Data}), which is the level of consensus results from processing the raw genome sequencing reads (Data Level 1 \textit{Raw Data}). This data includes 31 cancer types, listed in \tbref{Tab-TCGA_Cancers}. The remaining two profiles from TCGA, Mesotheliomia (MESO) and Acute Myeloid Leukemia (LAML), were excluded due to using an older version of the human reference genome at the time. It should also be noted that in this analysis Colon adenocarcinoma [COAD] and Rectum adenocarcinoma [READ] are combined into a single Colon and Rectum adenocarcinoma [COADREAD] profile, also Esophageal carcinoma [ESCA] and Stomach adenocarcinoma [STAD] are combined into a single Stomach and Esophageal carcinoma [STES] profile. These combination are due to the cancer mutation profiles being indistinguishable from one another \cite{Muzny2012,Bass2014}.

\comment{General note to self to mention all non-standard R and python packages used in my analysis}
%----------------------------------------------------------------------------------------


\begin{table}
\centering
\caption[TCGA Cancers in this Study]{The 31 cancer types involved in this study. COAD and READ were combined because their mutation profiles are indistinguishable \cite{Muzny2012}, while STES was not part of the original pilot project, but was investigated by \textcite{Bass2014} and subsequently added to TCGA. STES is a combination of two cancers, stomach and esophageal carcinomas into one unified mutation profile.
Number of subjects is based off of unique TCGA barcodes within each cancer dataset.
% Data from \url{https://tcga-data.nci.nih.gov/docs/publications/tcga/}
% Since it was downloaded through the Broad Institute,
% Better source of information: \url{https://gdac.broadinstitute.org/}
}
\label{Tab-TCGA_Cancers}
\begin{small}
\begin{tabular}{|l|>{\raggedright}p{0.50\textwidth}|l|} \hline
% Header
\multicolumn{1}{|c|}{\bfseries Identifier} &
\multicolumn{1}{|c|}{\bfseries Cancer Type} & 
\multicolumn{1}{|c|}{\bfseries Number of Subjects} \nl
% End header
ACC & Adrenocortical Carcinoma & 90 \nl

BLCA & Bladder Urothelial Carcinoma & 130 \nl
BRCA & Breast Invasive Carcinoma & 987 \nl

CESC & Cervical Squamous Cell Carcinoma and Endocervical Adenocarcinoma & 194 \nl
CHOL & Cholangiocarcinoma & 35 \nl
COADREAD & Colon Adenocarcinoma [COAD] \& Rectum Adenocarcinoma [READ] & 295 \nl

DLBC & Lymphoid Neoplasm Diffuse Large B-cell Lymphoma & 48 \nl

ESCA & Esophageal Carcinoma & 185 \nl

GBM & Glioblastoma Multiforme & 290 \nl

HNSC & Head and Neck Squamous Cell Carcinoma & 279 \nl

KICH & Kidney Chromophobe & 66 \nl
KIRC & Kidney Renal Clear Cell Carcinoma & 411 \nl
KIRP & Kidney Renal Papillary Cell Carcinoma & 161 \nl

LGG & Brain Lower Grade Glioma & 286 \nl
LIHC & Liver Hepatocellular carcinoma & 198 \nl
LUAD & Lung Adenocarcinoma & 230 \nl
LUSC & Lung Squamous Cell Carcinoma & 177 \nl

OV & Ovarian Serous Cystadenocarcinoma & 142 \nl

PAAD & Pancreatic Adenocarcinoma & 150 \nl
PCPG & Pheochromocytoma and Paraganglioma & 184 \nl
PRAD & Prostate Adenocarcinoma & 332 \nl

SARC & Sarcoma & 247 \nl
SKCM & Skin Cutaneous Melanoma & 345 \nl
STES & Stomach and Esophageal Carcinoma & 473 \nl

TGCT & Testicular Germ Cell Tumors & 155 \nl
THCA & Thyroid Carcinoma & 405 \nl
THYM & Thymoma & 118 \nl

UCEC & Uterine Corpus Endometrial Carcinoma & 247 \nl
UCS & Uterine Carcinosarcoma & 57 \nl
UVM & Uveal Melanoma & 80 \nl
\end{tabular}
\end{small}
\end{table}


%----------------------------------------------------------------------------------------


\subsection{Dataset Size}
The TCGA dataset used contained information on $95,836$ isoforms from $31$ cancer types, each combination of which was processed positionally and regionally for a total of three disorder score profiles:
\begin{enumerate*}
\item IUPred 'short' (positional), 
\item IUPred 'long' (positional), and 
\item FoldIndex\textcopyright (regional)
\end{enumerate*}. 


%----------------------------------------------------------------------------------------
%	Disorder Scoring
%----------------------------------------------------------------------------------------
\section{Disorder Scoring}
Positional analysis was done on measurements by IUPred long, or a $100$ residue interaction window, and IUPred short, or a $25$ residue interaction window \cite{Doszt??nyi2005}; regional analysis was done on measurements by FoldIndex\textcopyright \cite{Prilusky2005}, which uses a default window size of $51$ residues. Calculations for both positional measurements are based on pairwise chemical interaction energies across their respective window sizes and smoothed over a window size of $21$ residues -- this is in accordance with the IUPred method \cite{Dosztanyi2005a}. The positional long and short measurements are processed concurrently, but separately at each step. Calculations for regional measures are based on the Kyte/Doolittle scale\cite{Kyte1982} of hydrophobicity and net charge, considering the mean of both values across the window -- this is in accordance with the FoldIndex\textcopyright method \cite{Prilusky2005}.


%----------------------------------------------------------------------------------------
%	Monte Carlo Simulation
%----------------------------------------------------------------------------------------
\section{Monte Carlo Simulations}
Beginning with calculating IUPred long and IUPred short disorder score profiles for each protein isoform, Monte Carlo simulations were carried out by comparing the observed mutation load (see \eqref{Eq-ObsMut}) against the average mutation load across one million random simulations of the same number of mutations and calculating an empirical p-value (see \eqref{Eq-EmpiricalP}) between these values. The empirical p-value is the number of simulated cases below the observed disorder load divided by the number of simulations performed -- this calculation is based on comparing the observed value versus the simulated values if the null hypothesis is rejected (random mutations).


\begin{minipage}{\linewidth}
\begin{equation}
\label{Eq-EmpiricalP}
p_{positive} = \frac{\sum{M_{obs} \geq M_{random}}}{L_{random}}
\end{equation}
\singlespacing
\small
Where $p_{positive}$ is the empirical p-value for positive selection for disorder, $M_{obs}$ is the observed disorder load, $M_{random}$ is the vector of simulated disorder loads, and $L_{random}$ is the length of the simulated disorder loads vector. $L_{random}$ is equal to one million for each isoform.
\end{minipage}
\smallskip

\begin{minipage}{\linewidth}
\begin{equation}
\label{Eq-ObsMut}
M_{obs} = \sum_{i=1}^N{m_i \times s_i}
\end{equation}
\singlespacing
\small
Where $M_{obs}$ is the observed disorder load, $m_i$ is the number of observed mutations at position $i$, $s_i$ is the calculated IUPred disorder score at position $i$, and $N$ is the total number of residues in the protein.
\end{minipage}
\smallskip

%\begin{minipage}{\linewidth}
%\begin{equation}
%\label{Eq-AvgMut}
%M_{avg} = \frac{1}{1,000,000}\sum_{j=1}^{1,000,000}{m_i \times s_i}
%\end{equation}
%\singlespacing
%\small
%Where $j$ is the sample number, $m_i$ is the number of mutations placed at position $i$ in sample number $j$, and $s_i$ is the calculated IUPred disorder score at position $i$. $1,000,000$ replications were performed for each isoform-cancer combination using sampling with replacement to place the same number of observed mutations randomly within the sequence.
%\end{minipage} \\

Following empirical p-value calculation, one isoform per gene was selected according to the highest number of mutations and shortest protein length with any ties resolved alphanumerically. This selection was to ensure statistical independence prior to multiple hypothesis correction -- which was performed at a false discovery rate (FDR) level of $0.05$ using the Benjamini-Hochberg correction procedure to control FDR \cite{Benjamini1995b}. This selection was performed after p-value calculation rather than prior in order to test other potential avenues of investigation, such as single-gene isoform cross comparisons, which are not part of the work presented here.


\subsection{Steps as a List}
See also \fgref{Fig-PosStepsFlow} for these steps as a flowchart.

\begin{enumerate}
\item Calculate positional disorder scores via IUPred (long and short)
\item Simulate one million random mutation profiles using sampling with replacement (same number of mutations as observed)
	\begin{itemize}
	\item 'Observed' defined as individual mutated positions, not individual mutation observations so as to not inflate highly-mutated positions in analysis
	\end{itemize}
\item Calculate empirical p-value between observed and average random mutation load
\begin{samepage}
\item Select one isoform per gene
	\begin{itemize}
	\item[] Criteria:
		\begin{itemize}
		\item Highest number of mutations
		\item Shortest isoform length
		\item Ties resolved alphanumerically
		\end{itemize}
	\end{itemize}
\end{samepage}
\item Correct at FDR of $0.05$ according to Benjamini-Hochberg correction procedure
\end{enumerate}

\input{Chapters/partials/PosSteps.tex}


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\section{Binomial Testing}
\label{Sec-BinomialTesting}
First, disorder region calls for each protein isoform were made using the FoldIndex\textcopyright webserver\footnote{\url{http://bioportal.weizmann.ac.il/fldbin/findex}}. Following this, disordered regions within mutated isoforms for each cancer background were extracted. For each of these regions, five values were calculated to find regions with heightened mutational concentration:
\begin{enumerate*}
\item the total isoform length (length of the region as found via FoldIndex\textcopyright),
\item the total number of mutations observed in the isoform, 
\item the number of mutation observed in the disordered region, 
\item expected value (see \eqref{Eq-ExpVal}), and 
\item p-value via binomial test of observed number of mutations or fewer
\end{enumerate*}. See \fgref{Fig-RegStepsFlow} for a flowchart version of how these values are used.

Following binomial testing (see \eqref{Eq-BinomialDist}), the regions in each cancer were filtered for only the most significant isoform of each gene and the maximum number of results from a single cancer was limited to the top $100$ (both according to the lowest p-values). After this, adjusted p-values were calculated via FDR correction at $0.05$ level via the Benjamini-Hochberg correction procedure \cite{Benjamini1995b}. The filtering step ensures statistical independence, which is necessary for multiple hypothesis correct; the limiting step ensures meaningful validation (some cancers prior to limiting had in excess of $2,000$ significant genes).


\begin{minipage}{\linewidth}
\begin{equation}
\label{Eq-BinomialDist}
Pr(X = x) = {{n} \choose {x}} p^{x} (1 - p)^{n-x} 
\end{equation}
\singlespacing
\small
Where $Pr(X = x)$ is the probability of observing $x$ successes, $n$ is the number of trials (the length of the isoform), $x$ is the number of successes (the number of mutations in the region), and $p$ is the probability of success (the length of the region divided by the length of the isoform). For the work herein the binomial distribution density was used to calculate the probability observing exactly $x$ successes.
\end{minipage}
\smallskip

\begin{minipage}{\linewidth}
\begin{equation}
\label{Eq-ExpVal}
E_{val} = M \times \frac{len_{reg}}{len_{iso}}
\end{equation}
\singlespacing
\small
Where $E_{val}$ is the expected value, $M$ is the total number of observed mutations across the isoform, $len_{reg}$ is the length of the region, and $len_{iso}$ is the length of the isoform. This equals the number of mutations expected to randomly fall within the region.
\end{minipage}
\smallskip

\subsection{Steps as a List}
See also \fgref{Fig-RegStepsFlow} for these steps as a flowchart.

\begin{enumerate}
\item Calculate regional disorder scores via FoldIndex\textcopyright
\begin{samepage}
\item Run binomial tests to find regions with heightened mutational concentration
	\begin{itemize}
	\item Subset by $< -0.1$ average score in region
	\item Subset by greater than expected mutations given length of region, length of isoform, and number of observed mutations
	\item Subset by at least $5$ mutations in the region
	\end{itemize}
\end{samepage}
\item Find genes in common across cancers
%\begin{samepage}
%\item Determine if any regions have more concentrated mutations
%	\begin{itemize}
%	\item A significant result could be one mutation in a short region
%	\item Or equally many mutations in a longer region
%	\end{itemize}
%\end{samepage}
\begin{samepage}
\item Select one isoform per gene
	\begin{itemize}
	\item[] Criteria:
		\begin{itemize}
		\item Highest number of mutations
		\item Shortest protein length
		\item Ties resolved alphanumerically
		\end{itemize}
	\end{itemize}
\end{samepage}
\item Correct at FDR of $0.05$ according to Benjamini-Hochberg correction procedure
\end{enumerate}

\input{Chapters/partials/RegSteps.tex}
	
%----------------------------------------------------------------------------------------
%	SECTION 5
%----------------------------------------------------------------------------------------
\section{Enrichment Analysis and Validation}
The sets of significant genes from each method of analysis were run through enrichment analysis using hypergeometric testing across Gene Ontology Biological Process (GO-BP) terms with FDR correction. Biological process enrichment might suggest possible disorder-implicated mechanisms for driving cancer in yet uncharacterized proteins. The utilities used here for enrichment analysis were written in Python and R by my advisor prior to my work here. The Python script processes the raw annotation file to extract the GO branch of interest, in this case the Biological Process branch; in addition this, it also allows blacklisting evidence codes that would otherwise invite circular reasoning.\footnote{Using GO terms inferred by protein interaction would invite bias in the protein interaction partner sets created later for validation.} Then, in R, making heavy use of the \code{igraph} package many objects are generated: a GO graph, a GO dictionary, an annotation list, and a term-centric annotation list. Enrichment analysis was run both with and without FDR correction, in addition to filtering the results for only the most specific terms,\footnote{All parent terms in the GO-BP tree are removed, keeping only the most specific terms from within the tree.} however in all cases under-represented terms were thrown out.

To give mutation prevalence context to significant genes across cancer types, heatmaps were generated of cancer type versus significantly disorder-targeted genes with coloring by the ratio between the number of unique patients with a mutation in the gene over the number of unique patients in that cancer type. This allows cross-comparison of similar cancer types and similar genes as well as immediate validation that certain well-known outcomes are holding true (e.g., p53 should be significant across most cancers).

Due to the functional dependency between binding partners, a binding partners set is made and analyzed via enrichment analysis to determine if the partner set can provide additional insight into possible cancer drivers. 
Significant enrichment in the binding partners set would suggest mechanisms that are disrupted by disorder-targeting mutations.

Additional \textit{in silico} validation was done by ensuring a limited intersection between disorder-targeted sets and COSMIC, the Catalogue Of Somatic Mutations In Cancer \cite{Futreal2004a}. In addition to this limited intersection, a p-value for each significant set compared to the COSMIC census was computed via the hypergeometic distribution (see \eqref{Eq-phyper}) to find $P[X > x]$.

\begin{minipage}{\linewidth}
\begin{equation}
\label{Eq-phyper}
Pr(X = k) = {{{{K \choose {k}} {{N-k} \choose {n-k}}}} \over {{N \choose n}}}
\end{equation}
\singlespacing
\small
Where $N$ is the total population size (the number of genes in the TCGA set), $K$ is the number of successes in the population (the number of genes in the COSMIC set), $n$ is the number of draws (the number of of genes in each significant set), and $k$ is the number of successful draws (the number of genes in the intersect of COSMIC and each significant set).
\end{minipage}
\smallskip
