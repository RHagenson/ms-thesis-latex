% Secondary Intro Chapter reflecting meeting with advisor

\chapter{Introduction} % Main chapter title

\label{Ch-Intro} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Summary}
This work represents a focal shift in the process of \textit{in silico} discovery of cancer driver genes. Historically, there has been a general trend toward observing how DNA mutations propagate into disrupting ordered protein regions -- causing either an acceleration or deceleration of biochemical function, which can be linked to driving cancer. This focus is heavily influenced by the classic structure-function paradigm of proteins which states that a well-defined structure is necessary for well-defined function, or more simply that "structure dictates function" -- a paradigm supported by the seminal models of \textcite{Fischer1894} and \textcite{Pauling1951}, found to be true in early experimentally-determined structures \cite{Blake1965,Kendrew1961}, and lastly by the denaturation experiments of \textcite{Anfinsen1973}. This paradigm, although still with merit, gave rise to the long since disproven one-gene, one-protein belief that followed the characterization of DNA as the genetic material of the cell. However, now we know that a single gene can result in numerous different versions of a protein; these are known as protein \textit{isoforms} and are the result of primarily \textit{alternative splicing} \cite{Modrek2002,Kornblihtt2013,Black2003,Ast2004}. 

When considering mutations across isoforms, it is likely that a single mutation will affect isoforms differently. This is partially due to many isoforms being of different lengths -- thus one isoform may be shorter than sister isoforms due to removing a (mutated) region. As well, isoforms may be slight rearrangements of one another -- thus an isoform may splice a mutation into a new structural context. For the purposes herein, this differing nature of isoform mutations is not explored, rather the most mutated, shortest isoform of each gene is taken to represent the worst-case scenario. Proceeding in this way allows multiple hypothesis correction over statistically independent discrete tests while keeping computational and logical complexity lower for this initial focal shift within \textit{in silico} driver gene discovery.

%For the work herein, the max problem search space for significance is defined by the cross product of the $95,836$ isoforms, $31$ cancer types, $2$ background mutation profile types, and $3$ disorder score profiles. Not all of these combinations are explored, only those that are observed biologically. The analysis begins with data from The Cancer Genome Atlas (TCGA) containing observed DNA mutations in each of the $31$ cancer types. These observed DNA mutations are propagated to the protein/isoform level for continued analysis according to the known transcription and translation rules. These well-known rules mean it is possible to use "mutation" to mean the simultaneous change at the DNA and protein levels. Despite there being numerous ways to classify protein mutations, only two mutually exclusive classifications are used here: 
%\begin{enumerate*}
%\item synonymous mutation, no amino acid change despite a DNA mutation, and
%\item missense mutation, an amino acid change due to a DNA mutation
%\end{enumerate*}. From these mutation classifications, $2$ background profiles are created for each of the $31$ cancer types such that there are $31$ background profiles with all mutations (synonymous and missense) and $31$ background profiles with only missense mutations (no synonymous mutations) for a total of $62$ background profiles. The all-mutations profiles represent a na\"ive approach that makes no initial assumptions about how mutations will target regions of disorder, while the missense-only profiles represent the more biologically-relevant approach by assuming a change in amino acid must occur to have an appreciable effect. For these background profiles (two for each cancer), not all of the $95,836$ isoforms will have observed mutations for each cancer making this is the only dimension that changes on a per-cancer basis. For those cancer-isoform combinations that do exist, each is processed $3$ times via: 
%\begin{enumerate*}
%\item IUPred 'short', 
%\item IUPred 'long', and
%\item FoldIndex
%\end{enumerate*}, to create $3$ disorder score profiles. In summary, one combination of these dimensions might look like: protein \underline{ZZEF1} isoform \underline{1} in \underline{ACC} cancer considering \underline{only missense mutations} was processed by \underline{IUPred 'short'} to determine if there is positive selection for observed mutations targeting positions of heightened disorder.
%
%Following investigation of this search space it was found that 102 genes had mutations that significantly targeted positions of heightened disorder; after removing genes we understand the mechanism of this becomes 77 genes of unknown mechanism. The regional analysis is yet unknown\comment{Comment to draw attention to necessary later detailing.}.

By searching for a specific biological property within a large search space using data from the disease state of interest one can find novel genes that are implicated in that disease state via the property of focus. Using conservative statistical cutoffs, the confidence in those results is increased -- provided the biological property has a potential role in driving the disease state. Here, potential cancer driver genes are discovered by searching for protein disorder-targeting mutations across the largest public data search space. Protein disorder, a ubiquitous property within protein-protein interaction networks, has a strong potential role in driving cancer specifically by disrupting essential protein-protein interactions.


%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Cancer}
Cancer is a disease marked by the breakdown of cellular machinery due to somatic DNA mutations \cite{Hanahan2011}. The na\"{i}ve thought on potential driver genes of cancer would suggest they are as varied as the potential mutations which can occur in the variety of people it can occur within while still supporting life -- an impossibly large number of possibilities. Thankfully, this impossibly large number is not the true number of cancer drivers. Instead, there exist a much smaller number of drivers which can be roughly characterized as frequently-mutated or infrequently-mutated driver genes \cite{Vogelstein2013a}. 

The largest attempt to identify and record observed mutations in patients with cancer is The Cancer Genome Atlas (TCGA)\footnote{\url{https://cancergenome.nih.gov/}}. The TCGA datasets represent our most comprehensive observations to date despite being hardly a fraction of the total possibilities \cite{Tomczak2015}. It is unexpected to ever fully capture the true variety of cancer mutations with our observations due to the rarity of mutations and size of the human genome. The discrepancy between observations and potential drivers is where Bioinformatics and \textit{in silico} analysis can aid us. By comparing the small number of observations we do have against the large number of observations that are theoretically possible we can identify significant differences. This process is not without its own shortcomings as discussed in Section \ref{Sec-CompProb} below.

As a disease, cancer is especially complex due to the great variety of mutations that can occur, where they occur, which tissue(s) they are affecting, the individual’s personal genetics/diet/activity level, and even more factors having an effect on the disease progression \cite{Campisi2013,Lawrence2014a,Vogelstein2013a}. We know far more about cancer today than we ever have before due in no small part to our increasing ability to capture data on these factors and thereby understanding how they collectively contribute to cancer \cite{Chen2014,Cheng2014,Surget2013}. Although many people know of certain risk factors for cancer such as overexposure to UV radiation via sunlight, there are more subtle risk factors for cancer.

\subsection{Causes of Cancer}
Cancer being driven by mutations means that almost any aspect of life that causes mutations, prevents proper repair of genetic mistakes, or otherwise increases the mutation rate in a cell line can be linked to increased risk for developing cancer. This, quite thankfully, does not mean one \textbf{will} develop cancer, only that the risk is greater as carcinogenesis is a process not an event \cite{harrison2015}. 

Understanding cancer risk is made even more difficult by seemingly countering scientific findings. Nowhere is this more obvious than in layman news interpretations of the latest cancer findings stating that some commonplace routine such as drinking coffee increases cancer risk one day\footnote{\url{http://www.cnn.com/2016/06/15/health/coffee-tea-hot-drinks-cancer-risk/}} but previously decreased cancer risk\footnote{\url{http://www.cbsnews.com/news/new-findings-on-coffee-and-cancer-risk/}}. These news stories are often not overtly wrong, but are reductionist to the point of blurring the truth from the latest original scientific report \cite{loomis2016carcinogenicity}. The underlying truth is that the complex nature of oncogenesis can not so easily be linked to such commonplace activities because it is a disease of mutations and thus is different on a person-by-person, case-by-case basis with more important risk factors to consider than whether a person drinks coffee or not. However, there are such routines with undeniably strong evidence for causing cancer -- tobacco-use was epidemiologically-linked to increased risk for developing cancer and this fact is certainly not debated \cite{Boffetta2008,Denissenko1996,Vineis2004}. Ultimately, with cancer being driven by mutations it is important to understand the two major classes of mutagens: external to the body and internal to the body.

\subsubsection{Mutations from External Mutagens}
External mutagens are those which occur outside the body and affect the mutation rate inside the body. Even people without a biomedical research background understand these mutagens quite clearly and (most) take steps to avoid them. Often these are observed as chemicals that a person is exposed to or activities they willingly do that are linked with an increased risk for cancer. Examples include using tobacco \cite{DeMarini2004,Hecht1999} and increased exposure to UV radiation via sunlight \cite{DOrazio2013,DeGruijl1999}. Such activities causes chemical changes within the cell which lead to cellular mutations. Other commonly known external mutagens are radiation and chemical spills, which the fear of, this researcher believes, has led to increased environmental regulations to protect citizens from less easily self-prevented risk factors. The less-easily identified risks are no less important than the risks that are easily avoided.


\subsubsection{Mutations from Internal Mutagens}
Internal mutagens are those which occur inside the body to affect the mutation rate inside the body. Often these are far less understood by people without a biomedical research background and are often understated even by people with a biomedical research background. One internal mutagen is the DNA repair mechanisms failing to correct a mistake during DNA replication. In more detail, as DNA is replicated prior to cellular division, a complete copy must be made that will split off into the daughter cell following division. This replication process involves unzipping the DNA double helix via DNA helicase and building two new complementary strands via DNA polymerase. The average mistake rate for DNA polymerase is one in one hundred thousand $\left(\frac{1}{100,000}\right)$ positions. When we consider that there are roughly six billion ($6,000,000,000$) positions in a human diploid cell, this equals an average of one hundred twenty thousand ($120,000$) errors at each division \cite{Pray2008}. The cell is able to repair most, but not all, of these mistakes and if it cannot repair the mistakes should mark the cell for destruction as a major deviation from the healthy cell line. Any mistakes that are not corrected or deviated cell lines not terminated are, by definition, mutations and these mutations have the risk of being oncogenic.

Another internal mutagen is the progressive shortening of telomeres with each cell division and is partially why cancer is more common later in life. Telomeres are repeated, non-coding segments of DNA at the ends of chromosomes which protect the internal coding portions from mutations and degradation by being mutated and degraded themselves. As telomeres shorten with age, the coding portions are exposed to mutation and degradation \cite{Blasco2005}.


\subsection{Cancer Driver Genes}
There are two major classes of cancer driver genes: 
\begin{enumerate*}
\item tumor suppressor genes, and
\item oncogenes
\end{enumerate*} \cite{Weinberg1994,Lehman1991,Lee2010}. 

\subsubsection{Tumor Suppressor Genes}
Tumor suppressor genes are the "brakes" on tumorigenesis intended to stop the rapid cellular proliferation and growth characteristic of a tumor. These genes are a single point of failure following the Knudson two-hit hypothesis \cite{Knudson1971,Nordling1953,Hutchinson2001} and thus mutations within them tend to present fairly uniform results. 
This uniformity is a result of mutations having the same loss-of-function effect: preventing the gene from stopping the growth of tumors effectively, which presents the same no matter the causing mutation.
A notable example of a tumor suppressor genes is p53 (or TP53), which is ubiquitous and provides a check for deviated cell lines during the G1/S regulation point of the cell division cycle. Many mutations can result in p53 malfunction and that is why this driver gene is implicated in $>50\%$ of cancers \cite{Surget2013} -- it is a single point of failure where dysfunction means cell lines are not subjected to the proper health check prior to dividing.


\subsubsection{Oncogenes}
Oncogenes are the "gas" on oncogenesis with a variety of intended functions which are accelerated via mutation causing a variety of the notable cancer hallmarks. The diversity of these genes means there is greater diversity in their biochemical presentation. Newly discovered driver genes tend to fall into this class because their variety means different approaches analyze new contexts to how a gene might be driving oncogenesis. Oncogenes are set in motion by specific mutations while most mutations within them are random "passenger" mutations and not, themselves, oncogenic \cite{Weinberg1984,Chial2008,Todd1999,Stehelin1995}. A notable example of an oncogene is telomerase, which is oncogenic by causing cancer cells to lengthen their telomeres -- aiding in cancer cell immortality. 


\subsubsection{Discovering Drivers}
Discovering cancer driver genes requires many levels of analysis. Newer studies tend to look for positive selection for a biological property with potential in driving cancer. This is an effective combination of biological hypothesis and high-dimensional data analysis. In order to not bias results from these types of analyzes, capturing the mutational landscape of cancer must be done in as systematic a way as possible. Currently, the most systematic and comprehensive approach to discovering the mutations noted in cancer is The Cancer Genome Atlas (TCGA).


\subsection{The Cancer Genome Atlas}
The Cancer Genome Atlas (TCGA) is the leading effort to catalog genetic mutations in cancer via high-throughput genomics -- bettering our understanding of the genetic basis of cancer with a primary goal of improving diagnosis, treatment, and prevention of cancer. 
%According to The Cancer Genomes Atlas's About\footnote{\url{https://cancergenome.nih.gov/abouttcga}} page:
%
%\begin{displayquote}
%"The Cancer Genome Atlas (TCGA) is a collaboration between the National Cancer Institute (NCI) and the National Human Genome Research Institute (NHGRI) that has generated comprehensive, multi-dimensional maps of the key genomic changes in 33 types of cancer."
%\end{displayquote}

Over its lifespan from 2005 to 2017, it collected 2.5 petabytes of data from more than 11,000 patients describing the mutational observations of 33 cancer types. The data used in this study is from July 18th, 2016.
%with ongoing investigation in the following Analysis Working Groups (AWGs): 
%\begin{enumerate*}
%\item Liver hepatocellular carcinoma,
%\item Mesothelioma,
%\item Pancreatic ductal adenocarcinoma,
%\item Sarcoma,
%\item Testicular germ cell cancer,
%\item Thymoma,
%\item Uveal melanoma
%\end{enumerate*}

\subsubsection{Methods}
The TCGA Research Network consists of many parts; each part is integral to achieving TCGA's central goal -- beginning with the Biospecimen Core Resource (BCR), which reviews and processes the initial blood and tissue sample, and ending with the Analysis Working Groups (AWGs), which are made up of scientific and clinical experts analyzing a single type of cancer across all TCGA methods and who publish a comprehensive analysis of findings. 
%See \tbref{Tab-TCGA_Network} for more details on the TCGA Research Network.

%Seven facets of genomic data were captured for each pair of samples (tumor and control tissue); these are listed in \tbref{Tab-TCGA_Platforms}.


\subsubsection{Cancers in the Atlas}
Under TCGA investigation there are 33 tumor types (see \tbref{Tab-TCGA_Selected_Cancers}), of which 31 cancer types are included in this work (see \tbref{Tab-TCGA_Cancers}).
%Ten rare cancer listed in \tbref{Tab-TCGA_RareCancers} are included in TCGA -- one of which, mesothelioma (MESO), was not under analysis here. 
The two cancers present in TCGA not analyzed here are Mesothelioma (MESO) and Acute Myeloid Leukemia (LAML), which were excluded due to using an older version of the human reference genome at the time of SNP characterization.


%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------

\section{Computational Problem}
\label{Sec-CompProb}
The major computational problem within cancer genomics is distinguishing signal from noise -- driver mutation from passenger mutation -- which allows us to further understand the disease process. 

\subsection{Past Driver Gene Discovery Methods}
Detailing all past methods would be impossible, therefore a select few methods will be discussed. Past computational methods have focused within or integrated analysis in the areas of:
\begin{enumerate*}
\item somatic copy-number alternations (SCNAs), as is the case with GISTIC \cite{Mermel2011}; 
\item protein-coding region length, variations in mutation types, and multiple mutations in one gene, as is the case with DrGaP \cite{Hua2013}; and
\item signals of positive selection, as is the case with MuSiC \cite{Dees2012}, OncodriveFM \cite{Gonzalez-Perez2012}, OncodriveCLUST \cite{Tamborero2013a}, and E-Driver \cite{Porta-Pardo2014a}.
\end{enumerate*}. The methods leveraging positive selection all share the use of a background mutation profile/rate in order to differentiate between random mutation (passenger mutation) and driver mutation. Notably, none of these methods focus on investigating regions of disorder.


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Hypothesis}
I propose that by studying the effects of cancer mutations within inherently disordered regions, we can further understand how cancer manipulates cellular chemistry, disrupting healthy processes. Due to this being a major shift in focus from historical cancer driver gene discovery approaches, it is expected to find novel drivers.
