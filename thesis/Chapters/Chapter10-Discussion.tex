% Chapter Template

\chapter{Discussion} % Main chapter title

\label{Ch-Discussion} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	Introduction
%----------------------------------------------------------------------------------------
\section{Introduction}
Invariably if one attempts to interpret as many results as were found in the analysis here, $695$\footnote{$77$ from positional analysis, $623$ from regional analysis, with a $5$ result overlap; $77 + 723 - 5 = 695$}, without some systematic approach they would need to rely on some form of heuristic, of which there are no known heuristics in judging the significance or novelty of protein disorder findings within the context of cancer. Therefore, rather than random selection or taking the top N results from both methods to focus results it was decided to take the intersection of the two analysis methods to determine the most notable genes of potential disorder-driven cancer implication. Genes captured by both methods of analysis should have a heightened level of potential in driving cancer via the hypothesis herein that protein disorder may be implicated in yet uncharacterized driver genes.


%----------------------------------------------------------------------------------------
%	Intersection
%----------------------------------------------------------------------------------------
\section{Intersection of Both Methods of Analysis}
Given the positional and regional analysis methods each having their own bias -- positional will falsely call insignificant random mutations at disordered positions, regional analysis will call intrinsically disordered proteins significant due to being one large "disordered region" -- taking the intersection of the two novel find sets should give a high-confidence disorder-implicated set. The significant genes shared between the two method are listed in \tbref{Tab-IntersectResGenes}, note that only novel finds are considered here rather than all finds therefore none of these results are in the COSMIC census currently.

\begin{table}
\centering
\caption[Intersect of Positional and Regional Novel Finds]{The significant gene symbols according to both positional Monte Carlo simulations and regional binomial tests. Only those genes not already in the COSMIC census gene set are considered.
}
\label{Tab-IntersectResGenes}
\begin{scriptsize}
\begin{tabular}{|l|l|l|l|l|} \hline
EP400  &  
TBP  &  
SRRM2  &  
GPRIN2  &  
ZNF707 \nl
\end{tabular}
\end{scriptsize}
\end{table}

\subsection{EP400}
This gene, E1A-binding protein p400, is involved in the transcriptional activation of select genes via H4 and H2A acetylation \cite{Doyon2004}.\footnote{\url{http://www.uniprot.org/uniprot/Q96L91}} Notably, \textcite{Endo2013} found that this gene presented an ossifying fibromyxoid tumor. This was detected in only a single case, but shows potential reproducibility. The rarity and the uncertainty associated with the finding suggests it might be disorder-associated -- the rarity due to disorder regions being less susceptible to mutational disrupt, while the reproducibility suggesting it is more than a random chance finding. Meanwhile, \textcite{Mouradov2014} did a systematic investigation of primary colorectal tumors and compared them against TCGA data to conclude that these tumors are representative of the main subtypes of primary tumors at the genomic level -- finding EP400 mutation enrichment among other commonly found tumor genes. In addition to these findings, \textcite{Smith2010} and \textcite{Wu2015} found this gene implicated in human papillomavirus (HPV)-associated cancers and bladder cancer recurrence, respectively.

\subsection{TBP}
This gene, TATA-box-binding protein, is part of the TFIID complex and its binding to the complex is part of the initial transcriptional step of the pre-initiation complex (PIC).\footnote{\url{http://www.uniprot.org/uniprot/P20226}} 
TBP has not yet been implicated in cancer by itself, but has been noted in interaction with p53, a ubiquitous cancer driver gene.\footnote{\url{http://www.jbc.org/content/268/4/2284.short}}
This gene has primarily and almost exclusively been implicated in neurodegeneration, particularly via spinocerebellar ataxia.\footnote{\url{http://www.jbc.org/content/268/4/2284.short}}
If this gene is driving cancer via disorder-focused mutation it is likely affecting its ability to bind to the TFIID complex leading to a slowing of transcriptional activity or is affect the rate of signal transduction by p53 (GO:1901796).

\subsection{SRRM2}
This gene, Serine/arginine repetitive matrix protein 2, has been previously implicated in papillary thyroid carcinoma predisposition \cite{Tomsic2015}, colorectal cancer \cite{Hinoue2012}, and breast cancer \cite{Semaan2011}. Its exact function is still unknown, but it may stabilize the catalytic center or position of the RNA substrate being involved in pre-mRNA splicing\cite{Blencowe2000}.\footnote{\url{http://www.uniprot.org/uniprot/Q9UQ35}}


\subsection{GPRIN2}
This gene, G protein-regulated inducer of neurite outgrowth 2, was first shown to be involved in the G protein action of the brain \cite{Chen1999}. Since then is has been shown to be highly mutated in invasive lobular breast cancer \cite{Ciriello2015}, and involved in cancer risk in conjunction with environmental risks such as ceramic fibers \cite{Gerazime2016} and asbestos \cite{Jimenez2016}. Beyond these publications, GPRIN2 is mostly absent from any directed study.\footnote{\url{http://www.uniprot.org/uniprot/O60269/publications}}


\subsection{ZNF707}
This gene, zinc finger protein 707, has never been directly studied,\footnote{\url{http://www.uniprot.org/uniprot/Q96C28/publications}} instead all publications caught ZNF707 in other analyses with only one study mentioning it as a result. The study, by \textcite{Nesslinger2007}, found that in prostate cancer that ZNF707 \+ PTMA was recognized by treatment-associated autoantibodies. Beyond that ZNF707 has been annotated in four interactome studies \cite{Rual2005,Rolland2014,Hein2015,Xin2009}, sequenced as part of two analyses of chromosome 8 \cite{Nusbaum2006,Ota2004}, and part of an NIH project to expand the Mammalian Gene Collection (MGC) \cite{Gerhard2004}.


%----------------------------------------------------------------------------------------
%	Enrichment analysis
%----------------------------------------------------------------------------------------
\section{Enrichment Analyses}
\subsection{Positional}
The lack of significant terms following enrichment analysis does not elude meaning. A lack of enriched terms in this case might suggest that disorder-targeted proteins do not share a similar driving mechanism and instead are as varied as their lack of well-defined structure suggests. This varied set of mechanisms would likely be attributed to binding partner disruption if these disorder-targeted proteins are implicated in cancer. This aspect is supported by many of the uncorrected terms (\tbref{Tab-PosEA_Uncorrected}) being associated with complex protein network interactions.

The terms listed in \tbref{Tab-PosEA_NeighborsSpecific} (interaction partner set analysis) are all either associated with metabolic processes or gene expression. These associations are unsurprising given the mutations were noted in patients with cancer; however, more surprisingly, one of the top terms here is \textit{protein stabilization}, which might suggest that these disorder-targeted genes destabilize more than just their own binding relationships by having a secondary effect on protein stabilization at large. Another significant term, protein sumoylation, is a post-translational modification associated with apoptosis, protein stability, and progression through the cell cycle \cite{Hay2005} and is associated with the long-term fate of a protein.

\subsection{Regional}
The top 10 significant terms following FDR correction in \tbref{Tab-RegEA_Corrected} all having to do with metabolic regulation is unsurprising given the initial data was from cancer patients. This offers some validation to disorder being a potential driving mechanism for cancer as it appears to target genes with roles in classic cancer pathways.

The terms listed in \tbref{Tab-RegEA_NeighborsSpecific} suggest that disorder-implicated driver genes may drive cancer via their binding partners as opposed to directly driving cancer. Since every term in the table is concerning regulation, particularly of gene expression and biosynthesis, the effect(s) of mutations is likely to disrupt metabolic networks rather than metabolic processes directly. When looking at the full interaction partner set enrichment table (\tbref{Tab-RegFullNeighborTerms}), there are terms further down the list such as "positive regulation of ATP biosynthetic process" which suggest the energetics aspect of cancer induction. There are some surprising enriched terms such as "behavioral response to ethanol" which, despite being interesting, offer no aid in characterizing these genes as cancer driver genes -- rather they highlight the limitations of this approach (further discussed in \scref{Sec-Limitations}


\subsection{Regional and Positional Cross-comparison}
\subsubsection{Significant "Novel Finds" Sets}
Looking at both the uncorrected positional terms (\tbref{Tab-PosEA_Uncorrected}) and corrected regional terms (\tbref{Tab-RegEA_Corrected}) we see that the terms associated with genes found via regional analysis make more sense in driving cancer due to regulatory involvement, while the positional analysis has a great variety of terms that do not seem cancer-related. This discrepancy might indicate that cancer accumulates mutations in non-critical disordered regions causing these genes to being significant when looking for heightened mutational concentration, or it might indicate that positional analysis did not have a strong enough signal to find specific terms shared between disorder-implicated drivers if such drivers exist.


\subsubsection{Binding Partner Sets}
Between both positional (\tbref{Tab-PosEA_NeighborsSpecific}) and regional analysis (\tbref{Tab-RegEA_NeighborsSpecific}) partner set enrichment sets terms such as "protein sumylation"\comment{Check that I use quotes around terms such as these rather than italics, more proper that way.} and "protein stabilization" occur. This helps cross-validate the results from each method of analysis, however might also be due to the scale-free property of protein-protein interactions networks where gathering the interaction partner set to any initial set is likely to result in a more central set overall, in this case a more biologically critical gene set. This point is discussed further in \scref{Sec-Limitations} below.


%----------------------------------------------------------------------------------------
%	Disorder Binding Incitation of Cancer
%----------------------------------------------------------------------------------------
\section{Disorder Binding Incitation of Cancer}
Following the analysis herein, I suspect now that if disorder-implicated driver genes exist they are likely effecting cancer via their binding relationships. Disordered proteins add a robustness to protein-protein interaction networks by complementing the rigidity of ordered regions (e.g., bindings site and conserved domains). An ordered site being made more disordered by disrupting binding makes general sense, meanwhile the analysis herein did not offer any aid in answering the more general question of how disorder may incite cancer. It is possible that disorder-targeted genes incite cancer by affecting binding relationships rather than directly. However, further research needs to be done on how mutations in disordered regions of even known driver genes presents.


%%----------------------------------------------------------------------------------------
%%	COSMIC
%%----------------------------------------------------------------------------------------
\section{COSMIC -- Limited Complement}
The consensus driver genes in COSMIC have largely been determined by methods more geared toward finding order-targeted mutation effects and therefore offers a strong complement to the disorder-focused discovery of driver genes herein. Also, since COSMIC is the standard for causally implicated genes in cancer ensuring a degree of union here offers slight support for the remaining significant results being true positives. However, since the biological property basis of prior methods and the work herein differ so greatly using COSMIC in this way and as the gold-standard in computing hypergeometric testing p-values may be a misuse of the set. I would not suspect COSMIC to contain disorder-implicated driver genes if such genes exist, as well I suspect if such driver genes exist they makeup a very small minority of driver genes. Therefore using the COSMIC set to remove known driver genes represents a good use to find novel results, however the set does not likely include many terms that would be found by the method of analysis used herein due to the focus on protein disorder. 


%----------------------------------------------------------------------------------------
%	On Limit to In Silico Analysis
%----------------------------------------------------------------------------------------
\section{On Limit to In Silico Analysis}
Despite all the \textit{in silico} validation methods used herein, future validation via wet lab experimentation, possibly through the use of pull-down assays, will be necessary. Pull-down assays are particularly fit to the nature of disorder-regions due to directly testing binding disruption -- a likely hypothesis for how disorder-targeting mutations might drive cancer.


%%----------------------------------------------------------------------------------------
%%	Positional and Regional Cross
%%----------------------------------------------------------------------------------------
%\section{Internal Cross-validation Usage}
%Finding the shared significant genes across both positional and regional analysis should create a much smaller set given the countering biases of both approaches. Positional analysis is more likely to call random mutations spread across a protein significant in the case of small pockets of disorder, while regional analysis is more likely to call a highly disordered protein significant due to not utilizing a baseline disorder across the protein in the binomial test.

%----------------------------------------------------------------------------------------
%	High number of regional results
%----------------------------------------------------------------------------------------
\section{On the High Number of Regional Results}
Having $1819$ genes be called significant in regional analysis, and the remaining $623$ following removal of well-characterized driver genes, suggests a potential problem with the null model used in this approach. This is simply too many results to conclude anything meaningful shared between findings. If we assume these results are problematic, or at least the FDR correction is proper and $\frac{1}{20}^{th}$ of the results are false discoveries, then the number of results are most likely inflated by one of two possibilities:
\begin{enumerate*}
\item these region-gene combinations accumulate non-fatal, non-significant mutations after oncogenesis (passenger mutation accumulation), or
\item these mutations are important and their accumulation in so many genes indicates a more important conclusion to be made with further analysis (unknown mutation accumulation)
\end{enumerate*}. The latter of these is, at best, blindly hopeful of the significance of my findings and lacks an effective next step toward this "important conclusion." Meanwhile, the former is far more likely and has multiple next steps that can be taken. One potential next step, informed by the work of \textcite{London2010}, is to consider the mutation of "hot spot" residues to find mutated regions which would show the most binding disruption due to mutation of these "hot spot" residues. I would suspect that, given the additional biological significance subsetting rather than statistical subsetting, reanalyzing regional heightened mutational concentration with the addition of added weight to the mutation of these "hot spot" residues would drastically reduce the number of false discoveries.


%----------------------------------------------------------------------------------------
%	Limitations
%----------------------------------------------------------------------------------------
\section{Limitations}
\label{Sec-Limitations}
As with any analysis, the approach taken herein has its flaws. Here I discuss the most important limitations and problems with the analysis herein, however these are certainly not the only limitations given the scale and dimensionality of this analysis. With so many discrete tests in Monte Carlo simulations, binomial tests, and a variety of places corrections could have been performed, but were not due to a seemingly safe assumption that it was not necessary\footnote{An example of such would be, during regional analysis, correcting for the number of regions in an isoform/gene prior to selecting the most representative isoform. It is certainly more significant if a protein has many disordered regions and all the mutations concentrate in one disordered region than a highly-mutated protein with one very large disordered region.} there is no doubt that there are more limitations than just the ones presented here.


\subsection{Impact of mutations}
The impact or context of mutations is not considered in this analysis. We know of many reasons certainly seemingly insignificant mutations are far more important than they measured via the simple math used herein. One such case is that transition (i.e., purine to purine and pyrimidine to pyrimidine DNA mutations) versus transversion (i.e., purine to pyrimidine and pyrimidine to purine DNA mutations) are not considered despite this researcher's knowledge that transitions occur at a much higher random rate than transversions ($\approx 3:1$ ratio) despite there being twice as many transversions than transitions.

In considering mutations here the method na\"{i}vely assumes either all mutations matter (hopeful, but likely not true) or that only missense mutations matter (also hopefully, but likely not true since synonymous mutations do have an impact on translation rates). This limitation can be addressed through use of either/or MutSig \cite{Beroukhim2007} and SIFT \cite{Mooney2005} methods.

Converting DNA mutations to protein level and analyzing the data generated at that point, then trying to draw general conclusions about the gene level from the signal at the protein level required some level of compromise in considerations such as these.


\subsection{Monte Carlo simulations side effect}
A side effect of the Monte Carlo analysis is that if observed mutated positions are all slightly more disordered than the rest of the isoform that isoform will be called significant without a true disorder-driven reasoning. This is partially addressed by the countering regional analysis which mitigates against these false positives (therefore an intersection of positional finds and regional finds is a far more confident set).


\subsection{Intersection of significance sets}
Taking the intersection of all-mutation and missense-only profiles within both positional and regional analysis (so the intersection of four sets: positional-all, positional-missense, regional-all, and regional-missense) should result in a far more confident set, however drawing conclusions from this set will be difficult. Is a significant isoform disordered overall with great peaks of order? Do all the mutations matter? Questions such as these will need to be addressed in further research.
 

\section{Conclusions}
I am currently unable to draw any general conclusions about disorder-implicated driver genes. I believe further stringency is necessary to draw meaningful conclusions about this potential driving biological property. Addressing some of the limitations as stated above in \scref{Sec-Limitations} should be the next step. If possible, initial wet-lab validation of the high-confidence set (the union of all analyses and methods) could inform a more advanced reanalysis of the data used herein by finding some general property or binding partner common to mutated version of the significant isoforms.

One hurdle to draw conclusions is simply having all the reporting done (e.g., readable heatmaps for regional analysis, the intersection of positional and regional analysis performed, and so on). Once all the reporting is done, I have a good idea of a few isoform shared between the two analyses that present potential truly "novel finds" with unknown mechanism, but known implication in cancer.