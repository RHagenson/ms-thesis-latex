% Chapter Template

\chapter{Proteins} % Main chapter title

\label{Ch-Proteins} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------
\section{Introduction}
% Why discuss proteins?
For this work, and others like it, analysis at the protein level is necessary so here a brief overview of proteins is presented to provide context to analysis. Without an understanding of proteins, the positive selection for a protein biological property and how such selection might driver cancer cannot be understood.

% What is a protein and where are the mutations from?
Proteins are biopolymers made up of a string of amino acids and are the actors of biochemical activity. They are important for driving cellular chemistry by catalyzing reactions, acting as signals for processes, providing structural support to cells, helping other proteins fold, and much more. 
As the final step in \textit{The Central Dogma of Molecular Biology}, or that: a gene coded in DNA is transcribed into RNA, which is then translated into protein, these functional biomolecules are responsible for nearly all biochemical activity within the cell. Due to this, proteins serve as the chemical carriers for DNA mutations -- often being the biological component enacting damage due to the mutation. A single gene in DNA can result in multiple related protein products -- these related products are called protein \textit{isoforms} produced by \textit{alternative splicing} \cite{Modrek2002,Kornblihtt2013,Black2003,Ast2004}. Therefore, a mutation at the DNA level is likely to affect more than one protein end product. 
%Recording and making sense of where and how DNA is mutated in cancer is the intent of The Cancer Genome Atlas (TCGA) -- a collaboration between the National Cancer Institute (NCI) and National Human Genome Research Institute (NHGRI).

\section{Protein Structure}
Protein structure is broken up into four categories: primary structure ($1^\circ$), secondary structure ($2^\circ$), tertiary structure ($3^\circ$), and quaternary structure ($4^\circ$), each level of the structure built off of the level before it. These levels are discussed in detail below.

\subsection{Amino Acid Structure}
Before discussing the levels of protein structure, it is important to understand the basic structure of amino acids, the repeating subunits of the protein biopolymer. All amino acids are composed of four components all bonded to a central carbon atom. These four components are: 
\begin{enumerate*}
\item a single proton/hydrogen atom ($H^+$), 
\item an amine functional group ($-NH_2$),
\item a carboxyl functional group ($-COOH$), and
\item most importantly, a side chain specific to each amino acid ($-R$)
\end{enumerate*}. The side chain identifies the amino acid as well as its chemistry (i.e., is it polar/non-polar, aromatic/aliphatic, charged/non-charged). See \tbref{Tab-20AAs} for how the chemistry differs between amino acids.

\subsection{Primary Structure ($1^\circ$)}
Proteins are made up of a string of individual amino acids. Within human biology, there are 20 common amino acids (listed in \tbref{Tab-20AAs}) which make up all proteins. The linear, string sequence of amino acids is the primary ($1^\circ$) protein structure. (This is the only one-dimensional protein structure and thus is the one most often used in bioinformatics.)

\begin{table}
\centering
\caption[The Twenty Common Amino Acids]{
A brief summary of the twenty common amino acids. Full name, shortened name, single letter code, and a broad chemical classification are included for each. Reorganization of table at: \url{http://wbiomed.curtin.edu.au/biochem/tutorials/AAs/AA.html}
}
\label{Tab-20AAs}
\begin{small}
\begin{tabular}{|l|l|l|} \hline
% Header
\multicolumn{1}{|l|}{\bfseries Full name} & 
\multicolumn{1}{|l|}{\bfseries Shortened name} & 
\multicolumn{1}{|l|}{\bfseries Single Letter Code} \nl
% End header
% non-polar, aliphatic residues
\multicolumn{3}{|c|}{\bfseries aliphatic (non-polar)} \nl
Glycine & Gly & G \nl
Alanine & Ala & A  \nl
Valine & Val & V \nl
Leucine & Leu & L \nl
Isoleucine & Ile & I \nl
Proline & Pro & P \nl

% aromatic residues
\multicolumn{3}{|c|}{\bfseries aromatic (non-polar)} \nl
Phenylalanine & Phe & F \nl
Tyrosine & Tyr & Y \nl
Tryptophan & Trp & W \nl

% polar, non-charged residues
\multicolumn{3}{|c|}{\bfseries polar, non-charged} \nl
Serine & Ser & S \nl
Threonine & Thr & T \nl
Cysteine & Cys & C \nl
Methionine & Met & M \nl
Asparagine & Asn & N \nl
Glutamine & Gln & Q \nl

% positively charged residues
\multicolumn{3}{|c|}{\bfseries positively charged} \nl
Lysine & Lys & K \nl
Arginine & Arg & R \nl
Histidine & His & H \nl

% negatively charged residues
\multicolumn{3}{|c|}{\bfseries negatively charged} \nl
Aspartate & Asp & D \nl
Glutamate & Glu & E \nl
\end{tabular}
\end{small}
\end{table}

\subsection{Secondary Structure ($2^\circ$)}
As the protein begins to fold, it interacts with other residues and the environment to take on localized, 3D conformations that reduce localized energy levels. These local conformations are considered the secondary ($2^\circ$) protein structure and include: alpha helices, beta sheets, and turns/loops. Of these secondary elements, only turns/loops are fairly disordered. 

\subsection{Tertiary Structure ($3^\circ$)}
As the protein forms its secondary structure and continues to fold, it will continually assume the lowest overall energy state possible\footnote{This is without considering the role of \textit{chaperone proteins}, which help proteins fold in ways that would otherwise be chemically unstable in the process.} until the entire protein has been folded. This final folded structure of one original primary sequence chain is considered the tertiary ($3^\circ$) structure. It is important to draw attention to a tertiary structure being one continuous amino acid chain that has taken on a 3D folded structure. The structure of some proteins ends at this level since it often  stable and functional.

\subsection{Quaternary Structure ($4^\circ$)}
Not all proteins have a quaternary structure. The quaternary ($4^\circ$) structure is formed from multiple independent amino acid chains interacting with one another to form a complex. Every quaternary structure is made up of multiple protein chains, each capable of independent folding into a tertiary structure, and interacting with one another to form a final, functional protein complex. 


%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{Protein Folding}
\label{Sec-ProteinFolding}
According to the hydrophobic collapse model of protein folding, proteins begin to fold as they are being synthesized. First forming localized secondary elements at one end prior to the synthesis of the terminal end. 
There are two primary chemical driving forces behind protein folding, in order of strength: 
\begin{enumerate*}
\item the burial of hydrophobic side chains away from the aqueous environment, termed the \textit{entropic penalty}, and
\item the reduction in total, solvent-accessible surface area
\end{enumerate*} \cite{Dill2008}. Due to these chemical drivers, proteins result in a hydrophobic core and a hydrophilic surface. However, sometimes burying hydrophobic amino acids is not possible, especially in the early stages of folding. 

If these hydrophobic amino acid side chains were left exposed it would result in protein aggregation via the same entropic penalty driving their burial -- hydrophobic amino acids on the surface of the synthesizing protein would be driven toward hydrophobic surface amino acids on other proteins rather than driven inward \cite{kessel2010introduction}. Such aggregation would present a major and highly prevalent problem if the folding process were entirely stochastic; however there exist \textit{chaperone proteins} which support and protect a protein as it folds \cite{garrett2013biochemistry}. Chaperone proteins lower the overall energy barrier allowing folding into lower energy states that would first require adopting a higher, unfavorable energy state \cite{Liu2016a,Hendrick1993} -- as would be the case in temporarily exposing hydrophobic amino acids to bury them further than before.


%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------
\section{Protein Mutation}
\label{Sec-ProteinMutation}
Proteins are very rarely mutated directly and when they are rarely remain in the cell long due to protein turnover replacing a mutated protein with a healthy protein. Rather, most protein mutations can be linked back to an original DNA mutation which propagated to the protein level. Structurally, a mutation can occur within ordered regions such as binding or catalytic sites or within disordered regions such as protein-protein interaction junctions (there are also transition regions between these two). Since every amino acid has a unique chemistry, protein mutations rarely result in the same level of functionality -- accelerating or stunting protein activity based on the healthy and mutated amino acid chemistry. 
There are many classifications of protein mutations, each with their own semantic weight, however herein only two mutually-exclusive classifications are used: 
\begin{enumerate*}
\item synonymous mutation, no amino acid change despite a DNA mutation, and
\item missense mutation, an amino acid change due to a DNA mutation
\end{enumerate*}. It has been shown that synonymous mutations can result in effects at the protein level \cite{Goymer2007,Hunt2014,Sauna2011} and even frequently drive cancer \cite{Supek2014a}. However, a stronger case can be made for how a missense mutation may be driving cancer due to perturbed chemistry, therefore in this study two background mutation profiles are explored: all mutations (synonymous and missense) and missense-only (no synonymous mutations). The natural third profile, synonymous-only, would be nearly uninterpretable in itself at this initial stage of analysis.


%----------------------------------------------------------------------------------------
%%	SECTION 5 %----------------------------------------------------------------------------------------
\section{Protein Disorder}
\label{Sec-Disorder}
% What is protein disorder?
Protein order/disorder is the measure of how well-defined the 3D conformational location of a given residue within the final folded protein is. An ordered region is one that adopts a well-defined 3D conformation, while a disordered region may adopt no apparent structure or many similar structures depending on cellular conditions. Protein regions are made up of discrete residues each with their own order/disorder. Each residue can have as many potential inter-residue interactions as there are other residues in the protein. The combination of amino acids interactions is what leads to the native, or biologically-functional, 3D structure of a protein -- balancing attractive and repulsive forces to form the final conformation. One way of measuring the disorder of a protein is to consider each potential pairwise interaction across the length of the protein. In a protein only $100$ amino acids in length, this would be $100 \choose 2$ or $4950$ possible pairwise interactions -- a number that grows quickly with a length of $200$ being $200 \choose 2$ or $19,900$ pairwise interactions. Realistically, most residues do not interact with most other residues therefore not all combinations must be considered -- in fact the na\"{i}ve method of considering all possible combinations leads to inaccurate measures of order/disorder by neglecting proximity entirely -- thus \textit{de novo} measures of disorder commonly use sliding windows which consider interactions only within a certain sequence proximity range.

Due to our knowledge that protein folding is driven by the burial of hydrophobic side chains and reduction of surface area (see \scref{Sec-ProteinFolding} for more detail), we can estimate the final folded tertiary structure \textit{in silico} based on known properties of each individual amino acid in the primary sequence. These estimations approximate protein disorder by assigning a value to how predictable each residue's position is in the final structure. The two chemical measures used herein to estimate protein disorder from the primary sequence are:
\begin{enumerate*}
\item pairwise amino acid interactions, and
\item hydrophobicity and net charge
\end{enumerate*}. Both of these have basis in measuring the favorability of amino acid interactions to predict how the primary sequence will form secondary structures and final tertiary structure.


\subsection{Pairwise Amino Acid Interactions}
The chemical natures of different amino acids generate either attractive (favorable) or repulsive (unfavorable) pairwise interactions. Two polar amino acids of opposite charge or two non-polar amino acids will have favorable interactions, while two polar amino acids with the same charge or a polar and non-polar amino acid pair will have unfavorable interactions. The IUPred method \cite{Dosztanyi2005a} used herein to measure positional disorder scores is based on the ENERGI method of determining pairwise amino acids interaction energy-like quantities created by \textcite{Thomas1996}.

Using pairwise interaction energies in this way allows each position within a protein sequence to be given a score that corresponds to how well we can predict the final 3D conformational location of that position. The IUPred method uses a scale from 0 to 1 with precision to the ten-thousandth decimal place where 0 is complete order and 1 is complete disorder \cite{Doszt??nyi2005}. This method of positional score determination was chosen for its ability to distinguish partially disordered proteins from fully disordered proteins and is currently one of the best methods for measuring positional disorder, outperforming DISOPRED2 \cite{Ward2004} and VL3-H \cite{Obradovic2003}, both of which use a trained artificial intelligence model for disorder determination.

\subsection{Hydrophobicity and Net Charge}
With the strongest driving force behind protein folding being the entropic penalty, which forces the burial of hydrophobic amino acids, measures of hydrophobicity and net charge (an effective estimator of hydrophilicity) provide strong correlation with the ordered/disordered nature of the final folded structure. A region of highly hydrophobic amino acids indicates the region will likely be membrane-bound and thus more likely to be ordered, while a mixed region (alternating hydrophobic residues and hydrophilic residues) is unlikely to be bound and thus more likely to be disordered. FoldIndex\textcopyright, a method by \textcite{Prilusky2005}, uses an algorithm by \textcite{Uversky2000} to define a boundary line between regions of folded order and unfolded disorder. Values from this method are bound between -1 and 1 with positive values begin likely folded (ordered) regions and negative values being likely unfolded (disordered) regions.