\documentclass[12pt,doc]{apa6}

%\usepackage[margin=1in]{geometry}

% Make use of Biber and BibLaTeX for APA 6th conformance
\usepackage[american]{babel}
\usepackage{csquotes}
\usepackage[style=apa,maxcitenames=2,backend=biber]{biblatex}
\DeclareLanguageMapping{american}{american-apa}
\addbibresource{./bibtex/lit.bib}

% Package odds and ends
\usepackage[breaklinks, hidelinks]{hyperref}
\usepackage{xcolor}
\usepackage{amsmath}

% Images
\usepackage{graphicx}
\graphicspath{ {./imgs/} }

% Define comment command
% Sample:
% \comment{Here is my comment}
% Produces a red footnote marker, with a red text footnote  
\newcommand\comment[1]{\textcolor{red}{{\footnote{\textcolor{red}{#1}}}}}

% Make creating tables easier
\def\nl{\\\hline}

% Redefine \cite{} to \autocite{}
\renewcommand\cite[1]{{\autocite{#1}}}

% Define title page 
\begin{document}
\title{Protein Disorder in Cancer and its Effect on Binding Relationships -- Proposal}
\author{Ryan A. Hagenson}
\affiliation{University of Nebraska -- Omaha}
\abstract{
Cancer is driven by DNA mutations that propagate to the protein level. As the workhorses of biochemical activity, proteins and their inter-binding relationships are key to proper function within the cell. These relationships  often involve two distinct regions of the protein structure: a binding site and a binding target. Binding sites in proteins are highly-ordered -- dependent upon precise folding for successful binding. Meanwhile, their binding targets are often intrinsically disordered regions (IDRs), which have imprecise folding while still achieving successful binding. There has been much investigation into how binding site mutations affect binding, however there is very little known about how mutations in IDR binding targets affect binding. I will investigate the extent to which IDRs are targeted and affected within 31 cancer types. By investigating this phenomenon, I can further uncover how cancer manipulates cellular chemistry with respect to these crucial binding relationships, potentially leading to new means of cancer intervention or discovery of new driver genes.
}
\shorttitle{Protein Disorder in Cancer -- Proposal}
\date{\today}

\maketitle

\section*{Introduction}
Previous studies investigating the effects of protein order with respect to cancer mutations have focused on highly-ordered regions; correctly proposing that mutations in highly-ordered positions result in disruption of binding due to binding sites being dependent upon their correct 3D orientation to function -- leading to both destabilization of the protein itself and binding relationships. Equally as important to study are the counter to such highly-ordered regions: intrinsically disordered regions (IDRs) -- which allow for robust activity within protein interaction networks by complementing the rigidity of binding sites \cite{Babu2012}. IDRs have long been all but ignored\footnote{Intrinsically disordered regions were first noted and the term coined in the 70s while investigating chemical ligands via nuclear magnetic resonance (NMR) imaging, however the use of NMR on proteins truly began with \citeauthor{Wuthrich2001} in \citeyear{Wuthrich2001}, after which IDRs in proteins are reported in the literature, but not extensively investigated.}, but with a recent surge of investigation their importance is only now being uncovered and discussed \cite{Oldfield2014}.


% IDRs have biological importance due to their synergy with highly-ordered regions providing this versatility of function and as structural signals for protein docking.

Focusing on how mutations in disorder regions affect binding with partners it is important to clarify language. Primarily that the \textit{region} in question can be spoken about in terms of proximity in three-dimensional space or \textit{region} can be taken to mean neighboring sequence positions. To be clear, proximity/proximal will be used to indicate positions are close to one another in the three-dimensional folded protein (tertiary/quaternary structure), while region will be used to indicate positions are close to one another in the linear sequence (primary structure). That being said, most IDRs are clustered in linear regions along protein termini \cite{Uversky2013}, which is to be expected when one considers that intermediate positions in a sequence are more likely to be within ordered regions and proximity by the fact that every protein must eventually fold in on itself due to the effects of hydrophobicity -- surrounding these intermediate positions and thereby making them more ordered by disallowing reorientation.
% Do I need a figure here to better present this idea?
 
I propose that by identifying any significant mutation targeting of IDRs, we can further understand how cancer manipulates cellular chemistry -- disrupting healthy, normal processes. Further understanding the relatively understudied half of the paramount binding relationship may lead to new means of cancer intervention or discovery of new driver genes.
%, such as stabilization of protein docking when the binding site is unaffected, but the target is no longer recognizable.


\section*{Background}
% What is cancer?
The hallmarks of cancer are widely studied and have become a means to understand and investigate cancer as a disease, but in the simplest terms cancer arises from somatic mutation events \cite{Hanahan2011}. Without mutations, cancer does not arise, so increased understanding of mutation events and their effects leads to improved outcomes for patients. Traditionally, binding site mutations were primary subjects of research due to their potential involvement in these hallmarks, which can be brought on by a binding site's precise folding being disrupted. Destabilizing mutations in key proteins can stunt or rapidly accelerate the function of biochemical pathways, leading to the symptoms of cancer \cite{Sabarinathan2016,Muller2013,Melton2015}. Most mutations are destabilizing, which makes investigating intrinsically disordered regions (IDRs) a point of interest -- how are disordered regions affected by mutations when mutations normally cause the region to become more disordered?

% What is a protein and where are the mutations from?
Proteins are the workhorses of biochemical activity; driving cellular chemistry by catalyzing reactions, acting as signals for processes, and much more. As the final step in what is termed \textit{The Central Dogma of Molecular Biology}, or that: a gene coded in DNA is transcribed into RNA, which is then translated into a protein. Therefore, proteins serve as identifiers for the effects of DNA mutations. A single gene in DNA can often result in multiple related protein products -- these related products are called protein isoforms.\footnote{This process is called \textit{alternative splicing} and is much more common in eukaryotes, such as mammals, than it is in prokaryotes, such as bacteria.} Therefore, a mutation at the DNA level is likely to affect more than one protein in the end. Recording and making sense of where and how cancer mutates DNA is the intent of The Cancer Genome Atlas (TCGA) -- a collaboration between the National Cancer Institute (NCI) and National Human Genome Research Institute (NHGRI).

% What is protein disorder?
Protein order/disorder is the measure of how well we know the 3D conformation at each amino acid/residue position within the folded protein. Each position can have as many potential inter-residue interactions as there are other residues in the protein. The combination of these relationships is what gives a protein its native, or biologically-functional, 3D conformation. One way of measuring the disorder of a protein is to consider each potential pairwise interaction across the length of the protein. However, since most positions do not interact with most other positions, this na\"{i}ve method is not only unnecessary, but leads to inaccurate measures of order. A more robust method developed by \citeauthor{Dosztanyi2005a}, originally designed to discriminate intrinsically unstructured proteins (IUPs) from globular proteins (well-structured proteins), uses a window size to determine how many other positions to consider pairwise interactions with and uses a summation of all interaction energies to represent the effect interactions have on a given position's disorder. Although not originally designed for it, a slight iteration on this method called IUPred is quite effective at distinguishing IDRs from ordered regions.

Protein order/disorder is easier to visualize if we related it to something on the macroscopic level. Consider the problem of headphones that have tied themselves into knots within one's pocket -- the knotted headphones are our protein. The knots will only release, or move at all in some cases, if we forcefully manipulate them, hence these are \textit{ordered regions} because we are certain about their structure. Meanwhile, the ear-buds or headphone jack are typically left loose and swing freely even when the rest of the cord is tied into knots, hence these are \textit{disordered regions} because we are uncertain about their structure.

% What constitutes significant disorder?
% Not considered in Background here since significance is not a Background issue.

%
% USE FUTURE TENSE!!!
%
\section*{Methods}
% Where is the data from?
\subsection*{TCGA Data}
\subsubsection*{Data Acquisition}
Cancer mutation data was previously obtained from the latest available TCGA\footnote{\url{https://cancergenome.nih.gov/}} run on July 18th, 2016 from the Broad Institute Firehose system\footnote{\url{http://firebrowse.org}}, in the form of Data Level 2 (\textit{Processed Data}), which is the level of consensus results from processing the raw genome sequencing reads (Data Level 1 \textit{Raw Data}). This data includes 31 cancer types, listed in Table \ref{tab:types}.

% Taken out because it is bulk that is not needed
% and marks nearly the final state of the initiative since it will officially close in early 2017.\footnote{\url{https://cancergenome.nih.gov/abouttcga/overview}}

\subsubsection*{Dataset Size}
The TCGA contains information on $95,836$ proteins, each of which will be processed twice: by IUPred "long", or a $100$ residue window for interactions, and by IUPred "short", or a $25$ residue window for interactions. Calculations from both of these residue windows are then smoothed over a window size of 21 residues in the IUPred method \cite{Dosztanyi2005a}. These long and short measures will be processed concurrently, but separately at each step.

\subsection*{Measure of Disorder}
% How was disorder measures?
IUPred, a web-intended implementation of the algorithm by \citeauthor{Dosztanyi2005a}, will be used to measure the positional disorder for each individual protein and its isoforms. Originally designed to discriminate intrinsically unstructured proteins (IUPs) from globular proteins (well-structured proteins), the algorithm uses a window size to determine how many other positions to consider pairwise interactions with and takes a summation of all interaction energies (previously determined experimentally by \citeauthor{Thomas1996}) to represent the effect the pairs of a given position has on its positional order/disorder. This method uses a scale from 0 to 1 with precision to the ten-thousandth, where 0 is complete order and 1 is complete disorder \cite{Doszt??nyi2005}. This method was chosen for its ability to distinguish partially disordered proteins from fully disordered proteins and is currently the best method for measuring positional disorder -- outperforming DISOPRED2 \cite{Ward2004} and VL3-H \cite{Obradovic2003}.

\subsection*{Statistical Methods}
The methods used to investigate the effects of cancer mutations on protein disorder is broken into two sections: analyzing the effects within disorder positions and analyzing the effects within disordered regions within a given cancer. Both inter- and intracancer analysis will be performed, testing for significantly affected proteins within a single cancer type and across all cancer types as listed in Table \ref{tab:types}. Determining significantly affected proteins across cancers will occur by FDR correction of protein isoform p-values determined within each individual cancer.


\subsubsection*{Disordered Positions}
The significance of positional mutations across proteins will first be investigated by Monte Carlo sampling to determine if cancer selectively targets disorder positions within certain proteins. By comparing the sum of the observed disorder, those positions where mutations are reported times their positional disorder, versus the sum of randomly sampling (with replacement) the same number of affected positions across the protein, an empirical p-value will be calculated. This process will be repeated for every isoform of every gene affected within a given cancer. Following the end of this sampling process the p-values must be corrected to determine their true significance, however since isoforms of the same gene product are not independent only a single isoform from each gene will be selected for correction. The selection criteria will be: highest number of mutations and shortest protein sequence length (with any ties resolved alphanumerically). This is a strong combination of selectors due to the number of mutations being our primary point of interest and the likelihood observing a high number of mutations directly proportional to the length of the protein sequence. Those isoforms that are both highly-mutated and shorter than their alternative forms are the most likely candidates to represent the worst effects of mutations in that gene. Correction of these select isoform empirical p-values will be done via False Discovery Rate (FDR) testing to determine which isoforms truly are significant within cutoff of $p_{adj} < 0.05$.


\subsubsection*{Disordered Regions}
The significance of disordered regions within a protein will be investigated similar to \citeauthor{Porta-Pardo2014a}, who used a binomial distribution to compare the likelihood of observing a given number of mutations inside and outside a given one-dimensional cluster within the protein sequence. In more detail, their method maps where mutational are observed to functional regions of the protein, uses a binomial test for bias toward mutations occurring inside a region versus outside that region, and finally uses an FDR test to correct for significance with a cutoff of $p_{adj} < 0.05$. This method is dependent upon knowing the functional regions of a protein (this is including IDRs), however IDRs are easily identified by their non-conformance to structure indicating this is an effective method for investigating these regions. Due to IDRs being clustered in 1D space by primarily being along protein termini \cite{Uversky2013}, there is no need to involve structural prediction or mapping to PDB X-ray crystallography structures as is necessary when investigating interactions between highly-ordered regions \cite{Ghersi2014}.

% How are averages being taken?

% Are disorder scores high disordered themselves (high entropy)?

\subsection{Enrichment Analysis and Validation}
Once the final set of significantly affected proteins is found, it is critical to determine if these proteins are enriched within a biochemical pathway to suggest the means by which they drive cancer. As well, the results should be validated experimentally in as many ways as possible -- extending to wet lab validation, if possible.

The first method of validation is enrichment analysis. By determining if the significant set is enriched within certain biochemical pathways, I can potentially determine the means by which these proteins drive cancer. With some cancer-driving proteins having unknown means, this analysis may lead to such assignment between these known cancer-related proteins and the means by which they cause cancer. If the result is an even spread of the significant set across pathways, then I will know that these disorder-targeted proteins are not limited to one means of driving cancer, but rather have a part to play in multiple means.

Due to the dependency between binding partners, I will also create a new set of those partners to the significant set and analyze those via enrichment analysis to determine if the partner set can tell us anything about the means by which these significantly disorder-targeted proteins drive cancer.

Additional validation will be done by ensuring there is limited union between this set and those sets determined by analysis of highly-ordered regions (i.e. binding sites). There are plenty of sets, each determined by different methods, to validate against \textit{in silico}, however results should be validated \textit{in vitro} as well. This same set validation will also be performed against the Cancer Census Genes database\cite{Futreal2004a}, the standard for causally implicated mutations in cancer, in reverse ensuring a high degree of union between the significant set and this causally implicated known set.


\section*{Expected Outcome and Future Directions}
From early preliminary testing in BRCA (breast cancer), it is expected we will find significantly affected proteins via disordered positions and disordered regions analysis within individual cancer types. 
%However, until the complete set is determined, the enrichment analysis cannot occur due to lacking statistical robustness. 

If new means of intervention are uncovered or new driver genes are discovered, this may lead to new methods to combat cancer or its symptoms. It is possible that the enrichment analysis will show these proteins are the cause of simply symptoms of cancer or show that these proteins drive processes such as metastasis, allowing for methods to develop to improve patient comfort or outcomes. 


%%% 
%%% Tables, Figures, and Equations
%%%
\begin{table}
\centering
\caption{The 31 cancer types involved in this study. COAD and READ were combined because their mutation profiles are indistinguishable \cite{Muzny2012}, while STES was not part of the original pilot project, but was investigated in \citeyear{Bass2014} by \citeauthor{Bass2014}. 
% Data from \url{https://tcga-data.nci.nih.gov/docs/publications/tcga/}
% Since it was downloaded through the Broad Institute,
% Better source of information: \url{https://gdac.broadinstitute.org/}
}
\label{tab:types}
\begin{small}
\begin{tabular}{|l|l|} \hline
% Header
\multicolumn{1}{|c|}{\bfseries Identifier} & \multicolumn{1}{|c|}{\bfseries Cancer Type} \nl
% End header
ACC & Adrenocortical carcinoma  \nl
BLCA & Bladder Urothelial Carcinoma  \nl
BRCA & Breast invasive carcinoma  \nl
CESC & Cervical squamous cell carcinoma and endocervical adenocarcinoma  \nl
CHOL & Cholangiocarcinoma  \nl
COADREAD & Colon adenocarcinoma [COAD] \& Rectum adenocarcinoma [READ]  \nl
DLBC & Lymphoid Neoplasm Diffuse Large B-cell Lymphoma  \nl
ESCA & Esophageal carcinoma  \nl
GBM & Glioblastoma multiforme \nl
HNSC &  Head and Neck squamous cell carcinoma \nl
KICH & Kidney Chromophobe \nl
KIRC & Kidney renal clear cell carcinoma \nl
KIRP & Kidney renal papillary cell carcinoma \nl
LGG & Brain Lower Grade Glioma \nl
LIHC & Liver hepatocellular carcinoma \nl
LUAD & Liver hepatocellular carcinoma \nl
LUSC & Lung squamous cell carcinoma \nl
OV & Ovarian serous cystadenocarcinoma \nl
PAAD & Ovarian serous cystadenocarcinoma \nl
PCPG & Pheochromocytoma and Paraganglioma \nl
PRAD & Prostate adenocarcinoma \nl
SARC & Sarcoma \nl
SKCM & Skin Cutaneous Melanoma \nl
STES & Stomach and Esophageal carcinoma \nl
TGCT & Testicular Germ Cell Tumors \nl
THCA & Thyroid carcinoma \nl
THYM & Thymoma \nl
UCEC & Uterine Corpus Endometrial Carcinoma \nl
UCS & Uterine Carcinosarcoma \nl
UVM & Uveal Melanoma \nl
\end{tabular}
\end{small}
\end{table}


% Add biblography
\newpage
\printbibliography

\end{document}
